<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 21/06/17
 * Time: 15:36
 */

use BatFish\Core\App;
use BatFish\Core\Layout\CoreLayout;


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if(isset($_SERVER['REQUEST_URI'])){

if (!file_exists(__DIR__ . '/' . $_SERVER['REQUEST_URI']))
{
  $_GET['_url'] = $_SERVER['REQUEST_URI'];
}
}
require_once('../vendor/autoload.php');

$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

(new App())->setConfig(
    ''
)->setAppPath(
    ''
)->setPublicPath(
    ''
)->setRootPath(
    ''
)->setLayout(
     CoreLayout::class
)->run();
