<?php

namespace BatFish\Core\Http;


use BatFish\Core\Domain\DomainAwareTrait;

class Request
{
    use DomainAwareTrait;
    protected $get;
    protected $request;
    protected $post;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->get = $_GET;

        $request = array();
        parse_str(file_get_contents('php://input'), $request);
        $this->request = $request;
        $this->post = $_POST;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function getGet($key, $default = null)
    {

        if (!empty($this->get[$key])) {
            return $this->get[$key];
        }
        return $default;
    }

    /**
     * @param mixed $get
     * @return $this
     */
    public function setGet($get)
    {
        $this->get = $get;
        return $this;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function getRequest($key, $default = null)
    {
        if (!empty($this->request[$key])) {
            return $this->request[$key];
        }
        return $default;
    }

    /**
     * @param mixed $request
     * @return $this
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed
     * @internal param null $default
     */
    public function getPost($key, $default = null)
    {
        if (!empty($this->post[$key])) {
            return  $this->post[$key];
        }
        return $default;
    }

    /**
     * @param array $post
     * @return $this
     */
    public function setPost($post)
    {
        $this->post = $post;
        return $this;
    }

    public function getRemoteIpAddress()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    public function getSander(){
       return $_SERVER['HTTP_REFERER'];
    }

}