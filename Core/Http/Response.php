<?php
namespace BatFish\Core\Http;

use BatFish\Core\Domain\DomainAwareTrait;

class Response
{

  private $headers = [];

  /**
   * @param $name
   * @param $value
   */
  public function addHeader($name, $value)
  {
    $this->headers[$name] = $value;
  }

  /**
   * @param $url
   */
  public function redirect(string $url)
  {
    $this->setHeader('Location', $url);
    $this->respond();
  }

  /**
   * @param $name
   * @param $value
   */
  public function setHeader($name, $value)
  {
    $this->headers[$name] = (string)$value;
  }

  /**
   *
   */
  protected function respond()
  {
    foreach ($this->headers as $headerType => $headerValue)
    {
      header($headerType . ': ' . $headerValue);
    }
    die();
  }

}