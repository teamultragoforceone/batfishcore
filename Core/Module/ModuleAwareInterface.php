<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 19/11/16
 * Time: 09:43
 */

namespace BatFish\Core\Module;

use BatFish\Core\Domain\ComponentInterface;

interface ModuleAwareInterface
{

  public function getModule(): ModuleInterface;

  public function setModule($module);
}