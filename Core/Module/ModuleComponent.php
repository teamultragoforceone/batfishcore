<?php

namespace BatFish\Core\Module;

use BatFish\Core\Domain\Component;

abstract class ModuleComponent extends Component  implements ModuleComponentInterface
{
  use ModuleAwareTrait;

}