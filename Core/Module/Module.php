<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 19/11/16
 * Time: 09:43
 */

namespace BatFish\Core\Module;

use BatFish\Core\Domain\Component;

abstract class Module extends Component implements ModuleInterface
{
  use ModuleAwareTrait;

}