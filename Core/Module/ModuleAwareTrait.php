<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 19/11/16
 * Time: 09:43
 */

namespace BatFish\Core\Module;


trait ModuleAwareTrait
{
  protected $module;
  protected $service;

  public function getModule() : ModuleInterface
  {
    return $this->module;
  }

  /**
   * @param ModuleInterface $module
   * @return $this
   */
  public function setModule($module)
  {
    $this->module = $module;

    return $this;
  }

  public function getService()
  {
    return $this->module;
  }

  /**
   * @param ServiceInterface $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;

    return $this;
  }

}