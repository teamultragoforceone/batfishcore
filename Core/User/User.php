<?php
namespace BatFish\Core\User;

use BatFish\Core\Domain\ComponentInterface;
use BatFish\Core\Domain\DomainAwareTrait;

/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 05/07/17
 * Time: 02:23
 */
class User implements ComponentInterface
{
  use DomainAwareTrait;

  protected $currentUser;

  public function isLodgedIn()
  {

    if ($id = $this
      ->getDomain()
      ->getSession()
      ->get('id')
    ) {
      $currentUser =  $this->getDomain()->getModules()->getUserModule()->getById($id);
      if($currentUser->getGuid()){
        $this->setCurrentUser($currentUser);
          return true;
      }

    }

    return false;
  }

  /**
   * @return \App\Module\UserModule\Entity\User
   */
  public function getCurrentUser()
  {
      if(!$this->currentUser instanceof User){
          $this->setCurrentUser(new \App\Module\UserModule\Entity\User());
      }
    return $this->currentUser;
  }

  /**
   * @param \App\Module\UserModule\Entity\User $currentUser
   */
  public function setCurrentUser($currentUser)
  {
    $this->currentUser = $currentUser;
  }
}