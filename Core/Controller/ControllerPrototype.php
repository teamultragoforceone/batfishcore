<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 19/11/16
 * Time: 00:03
 */

namespace BatFish\Core\Controller;


use BatFish\Core\Domain\DomainAwareInterface;
use BatFish\Core\Domain\DomainAwareTrait;
use BatFish\Core\View\ViewModel;
use ReflectionClass;
use ReflectionMethod;

/**
 * Class ControllerPrototype
 * @package BatFish\Core\Controller
 */
abstract class ControllerPrototype implements DomainAwareInterface
{
  use DomainAwareTrait;

  /** @var  ViewModel[] */
  protected $viewModels = [];

  /**
   * @return mixed
   */
  abstract public function get();

  /**
   * @return mixed
   */
  abstract public function post();

  /**
   * @return mixed
   */
  abstract public function put();

  /**
   * @return mixed
   */
  abstract public function delete();

  /**
   * @return ViewModel[]
   */
  public function getViewModels():array
  {
    return $this->viewModels;
  }

  /**
   * @param ViewModel[] $viewModels
   */
  public function setViewModels(array $viewModels)
  {
    $this->viewModels = $viewModels;
  }

  /**
   * @param ViewModel $viewModel
   * @return $this
   */
  public function addViewModel($viewModel)
  {
    $this->viewModels[] = $viewModel;
    return $this;
  }

  /**
   * @param string $viewModel
   * @return $this
   */
  public function removeViewModel($viewModel)
  {
    unset($this->viewModels[$viewModel]);
    return $this;
  }

  /**
   * @return mixed
   */
  abstract public function __clone();

    public function render(){
        http_response_code(200);
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, PATCH, DELETE');
        header('Access-Control-Allow-Headers: X-PINGARUNER');
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 1728000');
        header('Access-Control-Allow-Origin: http://localhost:3000');
        $data=[];
        if (func_num_args() === 1)
        {
            $data['data'] = func_get_arg(0);

        }
        else
        {
            $data['data'] = func_get_args();


        }

        $data['messages'] = $this->getDomain()->getFlash()->getMessages();
        $json = json_encode($data);
        if ($callback = $this->getDomain()->getRequest()->getGet('callback'))
        {
            header('Content-Type: application/javascript');
            echo "{$callback}({$json})";
        }
        else
        {

            header('Content-Type: application/json');

            echo $json;
        }

        exit;
    }
  protected function invokeController($controller, $method = '')
  {

    $controller = new $controller;

    $domainReflectionClass = new ReflectionMethod(new $controller, 'setDomain');
    $domainReflectionClass->invoke($controller, $this->getDomain());
    $methodReflectionClass = new ReflectionMethod(new $controller, $method);
    $methodReflectionClass->invoke($controller);
    return $methodReflectionClass;


  }

    protected function systemLog($message , $level = 'info'){
        $this->getDomain()->systemLog($message , $level);
    }
}