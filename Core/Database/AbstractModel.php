<?php

namespace BatFish\Core\Database;

use BatFish\Core\Collection\AbstractCollection;
use BatFish\Core\Collection\AbstractEntity;
use BatFish\Core\Collection\BlankEntity;
use BatFish\Core\Domain\DomainAwareTrait;
use MongoDB\Client;
use MongoDB\Collection;
use MongoDB\Database;
use MongoDB\Driver\Exception\InvalidArgumentException;
use MongoDB\Driver\Exception\RuntimeException;
use MongoDB\Exception\UnsupportedException;

/**
 * Class AbstractModel
 * @package BatFish\Core\Database
 */
abstract class AbstractModel implements ModelInterface
{
  use DomainAwareTrait;
  /** @var  AbstractEntity */
  protected $entity;
  /** @var  AbstractCollection */
  protected $collection;

  /**
   * @return \MongoDB\Driver\Cursor
   */
  public function find()
  {
    return $this->collection()->find(array_filter($this->deserialize()));
  }

  /**
   * @return Collection
   */
  abstract public function collection(): Collection;

  protected function deserializeAll()
  {
    $arr = [];
    foreach ($this->getCollection()->getAll() as $item)
    {
      $arr[] = $this->deserialize($item);
    }
  }

  /**
   * @return AbstractCollection
   */
  public function getCollection(): AbstractCollection
  {
    return $this->collection;
  }

  /**
   * @param AbstractCollection $collection
   */
  public function setCollection(AbstractCollection $collection)
  {
    $this->collection = $collection;
  }

  /**
   * @param null $ent
   *
   * @return array
   */
  protected function deserialize($ent = null)
  {

    if (!$ent)
    {

      $ent = $this->getEntity();

    }

    return $ent->jsonSerialize();
  }

  /**
   * @return AbstractEntity
   */
  public function getEntity(): AbstractEntity
  {
    return $this->entity;
  }

  /**
   * @param AbstractEntity $entity
   *
   * @return $this|AbstractModel
   */
  public function setEntity(AbstractEntity $entity): self
  {
    $this->entity = $entity;

    return $this;
  }

  /**
   * @return array|null|object
   * @throws UnsupportedException
   * @throws \MongoDB\Exception\InvalidArgumentException
   * @throws \MongoDB\Driver\Exception\RuntimeException
   */
  public function findOne()
  {

 $d = $this->collection()->findOne(array_filter($this->deserialize()));

    return$d;
  }

  /**
   * @return \MongoDB\InsertOneResult
   * @throws \MongoDB\Exception\InvalidArgumentException
   * @throws \MongoDB\Driver\Exception\RuntimeException
   */
  public function save()
  {

    return $this->collection()->insertOne($this->deserialize());
  }

  public function updateOne(AbstractEntity $entity) {

     $blankEntity = $this->setEntity(
         (new BlankEntity())->setGuid($entity->guid)
     )->findOne();
  return $this
       ->collection()
       ->replaceOne(
       $this->deserialize($blankEntity),
       $this->deserialize($entity)
   );


  }

  /**
   * @return Database
   */
  protected function getDatabase(): Database
  {
    return $this->getClient()->selectDatabase($this->getDomain()->getConfig()->get('app.defaults.database.name'));
  }

  /**
   * @return Client
   * @throws \MongoDB\Exception\InvalidArgumentException
   * @throws RuntimeException
   * @throws InvalidArgumentException
   */
  protected function getClient(): Client
  {
    return new Client('mongodb://localhost:27017');
  }

  /**
   * @throws  RuntimeException
   * @throws  InvalidArgumentException
   * @throws \MongoDB\Exception\InvalidArgumentException
   */
  protected function insert()
  {
    $this->collection()->insertOne($this->deserialize());
  }

  protected function updateAll() { }

  protected function serialize()
  {
  }

    /**
     * @param AbstractEntity|null $entity
     * @return array|null|object
     */
  public function delete(AbstractEntity $entity = null){

      if(empty($entity)){
          //$this->setEntity($entity);
          $entity = $this->getEntity();

      }
      $this->setEntity((new BlankEntity())->setGuid($entity->guid) )->findOne();

      return $this
        ->collection()
        ->findOneAndDelete(
            array_filter(
                $this->deserialize()
            )
        );
  }

}