<?php

namespace BatFish\Core;

use BatFish\Core\Config\Config;
use BatFish\Core\Domain\Component;
use Batfish\Core\Exceptions\BatfishLayoutException;
use BatFish\Core\Layout\CoreLayout;
use BatFish\Core\Route\Route;
use BatFish\Core\Route\Router;

class App extends Component
{

  protected $routes;

  protected $appPath;

  protected $publicPath;

  protected $rootPath;

  protected $config;

  /**
   * @var Router
   */
  protected $router;

  protected $routs;

  /**
   * App constructor.
   */
  public function __construct()
  {
  }

  /**
   * @return Config
   */
  public function getConfig():Config
  {
    return $this->config;
  }

  /**
   * @param Config $config
   * @return App
   */
  public function setConfig(Config $config)
  {
    $this->config = $config;

    $this
      ->getDomain()
      ->setConfig($config);

    return $this;
  }

  /**
   * @return mixed
   */
  public function getPublicPath()
  {
    return $this->publicPath;
  }

  /**
   * @param mixed $publicPath
   * @return App
   */
  public function setPublicPath($publicPath)
  {
    $this->publicPath = $publicPath;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getRootPath()
  {
    return $this->rootPath;
  }

  /**
   * @param mixed $rootPath
   * @return App
   */
  public function setRootPath($rootPath)
  {
    $this->rootPath = $rootPath;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getAppPath()
  {
    return $this->appPath;
  }

  /**
   * @param mixed $appPath
   * @return App
   */
  public function setAppPath($appPath)
  {
    $this->appPath = $appPath;
    return $this;
  }

  /**
   * @return array
   */
  public function getRoutes():array
  {
    return Route::getRoutes();
  }


  /**
   * @internal param $controller
   * @internal param $action
   */
  public function run()
  {
    $this->getRouter()->run();
  }

  /**
   * @return mixed
   */
  public function getRouter():Router
  {
    if (!$this->router instanceof Router)
    {
      $this->setRouter(
          (new Router())
            ->setDomain(
              $this->getDomain()
          )
      );
    }
    return $this->router;
  }

  /**
   * @param Router $router
   * @return App
   */
  public function setRouter(Router $router):App
  {
    $this->router = $router;
    return $this;
  }

  /**
   * @param string $layout
   *
   * @return $this
   * @throws BatfishLayoutException
   */
  public function setLayout($layout)
  {
      /** @var CoreLayout $layout */
      $layout = new $layout;

    $layout->setDomain($this->getDomain());
    $layout->init();
    if (!is_subclass_of($layout, CoreLayout::class))
    {
      throw new BatfishLayoutException('Layout must derive from CoreLayout.');
    }

    $this
        ->getRouter()
        ->setLayout(
      $layout
          ->setDomain(
          $this->getDomain()
      )
    );

    return $this;
  }
}
