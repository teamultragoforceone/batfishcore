<?php

namespace BatFish\Core\Session;

use BatFish\Core\Domain\DomainAwareInterface;

Interface SessionInterface extends DomainAwareInterface
{
  /**
   * @return bool
   */
  public function start();

  /**
   * @return int
   */
  public function status(): int;

  /**
   * @return array
   */
  public function getOptions(): array;

  /**
   * @param array $options
   *
   * @return mixed
   */
  public function setOptions(array $options);

  /**
   * @param string $name
   *
   * @return mixed
   */
  public function setName(string $name);

  /**
   * @return string
   */
  public function getName();

  /**
   * @param bool $deleteOldSession
   *
   * @return Session
   */
  public function regenerateId(bool $deleteOldSession = null): Session;

  /**
   * @return string
   */
  public function getId();

  /**
   * @param string $id
   *
   * @return mixed
   */
  public function setId(string $id);

  /**
   * @return bool
   */
  public function isStarted();

  /**
   * @param bool $removeData
   *
   * @return bool
   */
  public function destroy(boolean $removeData = null);

  /**
   * @param $haystack
   * @param $needle
   *
   * @return mixed
   */
  public function startsWith($haystack, $needle);

  /**
   * @param string $index
   *
   * @return mixed
   */
  public function __get(string $index);

  /**
   * @param string $index
   * @param        $value
   *
   * @return mixed
   */
  public function __set(string $index, $value);

  /**
   * @param string $index
   * @param null   $defaultValue
   * @param bool   $remove
   *
   * @return mixed
   */
  public function get(
    string $index,
    $defaultValue = null,
    boolean $remove = null
  );

  /**
   * @param string $index
   * @param        $value
   *
   * @return mixed
   */
  public function set(string $index, $value);

  /**
   * @param string $index
   *
   * @return bool
   */
  public function __isset(string $index);

  /**
   * @param string $index
   *
   * @return bool
   */
  public function has(string $index);

  /**
   * @param string $index
   *
   * @return mixed
   */
  public function __unset(string $index);

  /**
   * @param string $index
   *
   * @return mixed
   */
  public function remove(string $index);

  /**
   *
   */
  public function __destruct();

  /**
   * @param $haystack
   * @param $needle
   *
   * @return mixed
   */
  public function endsWith($haystack, $needle);
}
