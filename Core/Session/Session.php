<?php

namespace BatFish\Core\Session;

use BatFish\Core\Domain\DomainAwareTrait;

class Session implements SessionInterface
{
  use DomainAwareTrait;
  const SESSION_ACTIVE = 2;

  const SESSION_NONE = 1;

  const SESSION_DISABLED = 0;
  /** @var  string */
  protected $uniqueId;
  /** @var  bool */
  protected $started = false;
  /** @var  array */
  protected $options;

  /**
   * Phalcon\Session\Adapter constructor
   *
   * @param array $options
   */
  public function __construct(array $options = [])
  {
    $this->setOptions($options);
  }

  /**
   * Starts the session (if headers are already sent the session will not be
   * started)
   */
  public function start()
  {

   if (!headers_sent())
    {
      if (!$this->started && $this->status() !== self::SESSION_ACTIVE)
      {
        session_start();
        $this->started = true;

        return true;
      }
    }

    return false;
  }

  /**
   * Returns the status of the current session.
   *<code>
   *    var_dump($session->status());
   *  if ($session->status() !== $session::SESSION_ACTIVE) {
   *      $session->start();
   *  }
   *</code>
   */
  public function status(): int
  {

    $status = session_status();

    switch ($status)
    {
      case PHP_SESSION_DISABLED:
        return $this::SESSION_DISABLED;
      case PHP_SESSION_ACTIVE:
        return $this::SESSION_ACTIVE;
    }

    return self::SESSION_NONE;
  }

  /**
   * Get internal options
   */
  public function getOptions(): array
  {
    return $this->options;
  }

  /**
   * Sets session's options
   *<code>
   *    $session->setOptions(['uniqueId' => 'my-private-app']);
   *</code>
   *
   * @param array $options
   *
   * @return mixed|void
   */
  public function setOptions(array $options)
  {
    if (isset($options['uniqueId']))
    {
      $this->uniqueId = $options['uniqueId'];
    }

    $this->options = $options;
  }

  /**
   * Set session name
   *
   * @param string $name
   *
   * @return mixed|void
   */
  public function setName(string $name)
  {
    session_name($name);
  }

  /**
   * Get session name
   */
  public function getName()
  {
    return session_name();
  }

  /**
   * {@inheritdoc}
   */
  public function regenerateId(bool $deleteOldSession = null): Session
  {
    session_regenerate_id($deleteOldSession);

    return $this;
  }

  /**
   * Returns active session id
   *<code>
   *    echo $session->getId();
   *</code>
   */
  public function getId()
  {
    return session_id();
  }

  /**
   * Set the current session id
   *<code>
   *    $session->setId($id);
   *</code>
   *
   * @param string $id
   *
   * @return mixed|void
   */
  public function setId(string $id)
  {
    session_id($id);
  }

  /**
   * Check whether the session has been started
   *<code>
   *    var_dump($session->isStarted());
   *</code>
   */
  public function isStarted()
  {
    return $this->started;
  }

  /**
   * Destroys the active session
   *<code>
   *    var_dump($session->destroy());
   *    var_dump($session->destroy(true));
   *</code>
   *
   * @param bool $removeData
   *
   * @return bool
   */
  public function destroy(boolean $removeData = null)
  {

    if ($removeData)
    {
      $uniqueId = $this->uniqueId;
      if (!empty ($uniqueId))
      {
        foreach ($_SESSION as $key)
        {
          if ($this->startsWith($key, $uniqueId . '#'))
          {
            unset ($_SESSION[$key]);
          }
        }
      }
      else
      {
        $_SESSION = [];
      }
    }

    $this->started = false;

    return session_destroy();
  }

  /**
   * @param $haystack
   * @param $needle
   *
   * @return bool
   */
  public function startsWith($haystack, $needle)
  {
    $length = strlen($needle);

    return (substr($haystack, 0, $length) === $needle);
  }

  /**
   * Alias: Gets a session variable from an application context
   *
   * @param string $index
   *
   * @return null
   */
  public function __get(string $index)
  {
    return $this->get($index);
  }

  /**
   * Alias: Sets a session variable in an application context
   *
   * @param string $index
   * @param        $value
   *
   * @return mixed|void
   */
  public function __set(string $index, $value)
  {
    return $this->set($index, $value);
  }

  /**
   * Gets a session variable from an application context
   *<code>
   *    $session->get('auth', 'yes');
   *</code>
   *
   * @param string $index
   * @param null   $defaultValue
   * @param bool   $remove
   *
   * @return null
   */
  public function get(
    string $index,
    $defaultValue = null,
    boolean $remove = null
  )
  {
    $uniqueId = $this->uniqueId;
    $key = $index;
    if (!empty ($this->uniqueId))
    {
      $key = $uniqueId . '#' . $index;
    }

    if (isset($_SESSION[$key]))
    {
      if ($remove)
      {
        unset ($_SESSION[$key]);
      }

      return $_SESSION[$key];
    }

    return $defaultValue;
  }

  /**
   * Sets a session variable in an application context
   *<code>
   *    $session->set('auth', 'yes');
   *</code>
   *
   * @param string $index
   * @param        $value
   *
   * @return mixed|void
   */
  public function set(string $index, $value)
  {
    if (!empty ($this->uniqueId))
    {
      $_SESSION[$this->uniqueId . '#' . $index] = $value;

      return;
    }

    $_SESSION[$index] = $value;
  }

  /**
   * Alias: Check whether a session variable is set in an application context
   *
   * @param string $index
   *
   * @return bool
   */
  public function __isset(string $index)
  {
    return $this->has($index);
  }

  /**
   * Check whether a session variable is set in an application context
   *<code>
   *    var_dump($session->has('auth'));
   *</code>
   *
   * @param string $index
   *
   * @return bool
   */
  public function has(string $index)
  {

    if (!empty ($this->uniqueId))
    {
      return isset ($_SESSION[$this->uniqueId . "#" . $index]);
    }

    return isset($_SESSION[$index]);
  }

  /**
   * Alias: Removes a session variable from an application context
   *
   * @param string $index
   *
   * @return mixed|void
   */
  public function __unset(string $index)
  {
    return $this->remove($index);
  }

  /**
   * Removes a session variable from an application context
   *<code>
   *    $session->remove('auth');
   *</code>
   *
   * @param string $index
   *
   * @return mixed|void
   */
  public function remove(string $index)
  {

    if (!empty ($this->uniqueId))
    {
      unset ($_SESSION[$this->uniqueId . '#' . $index]);

      return;
    }

    unset ($_SESSION[$index]);
  }

  public function __destruct()
  {
    if ($this->started)
    {
      session_write_close();
      $this->started = false;
    }
  }

  public function endsWith($haystack, $needle)
  {
    $length = strlen($needle);
    if ($length == 0)
    {
      return true;
    }

    return (substr($haystack, -$length) === $needle);
  }
}