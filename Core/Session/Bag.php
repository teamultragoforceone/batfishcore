<?php

namespace BatFish\Core\Session;

use ArrayAccess;
use ArrayIterator;
use BatFish\Core\Domain\DomainAwareTrait;
use Countable;
use IteratorAggregate;

/**
 * This component helps to separate session data into "namespaces". Working by
 * this way you can easily create groups of session variables into the
 * application
 *<code>
 *    $user = new \BatFish\Session\Bag('user');
 *    $user->name = "Kimbra Johnson";
 *    $user->age  = 22;
 *</code>
 */
class Bag implements BagInterface, IteratorAggregate, ArrayAccess, Countable
{
  use DomainAwareTrait;

  /**
   * @var string
   */
  protected $name;
  /**
   * @var mixed
   */
  protected $data;
  /**
   * @var bool
   */
  protected $initialized = false;

  /**
   * @var SessionInterface
   */
  protected $session;

  /**
   * @param string $name
   */
  public function __construct(string $name)
  {
    $this->name = $name;
  }

  /**
   * Destroys the session bag
   *<code>
   * $user->destroy();
   *</code>
   */
  public function destroy()
  {
    if ($this->initialized === false)
    {
      $this->initialize();
    }
    $this->data = [];
    $this->session->remove($this->name);
  }

  /**
   * Initializes the session bag. This method must not be called directly, the
   * class calls it when its internal data is accessed
   */
  public function initialize()
  {

    $session = $this->session;

    $data = $session->get($this->name);
    if (!$data)
    {
      $data = [];
    }

    $this->data = $data;
    $this->initialized = true;
  }

  /**
   * Magic getter to obtain values from the session bag
   *<code>
   * echo $user->name;
   *</code>
   *
   * @param string $property
   *
   * @return mixed
   */
  public function __get(string $property)
  {
    return $this->get($property);
  }

  /**
   * Magic setter to assign values to the session bag
   *<code>
   * $user->name = "Kimbra";
   *</code>
   *
   * @param string $property
   * @param        $value
   *
   * @return mixed|void
   */
  public function __set(string $property, $value)
  {
    $this->set($property, $value);
  }

  /**
   * Obtains a value from the session bag optionally setting a default value
   *<code>
   * echo $user->get('name', 'Kimbra');
   *</code>
   *
   * @param string $property
   * @param null   $defaultValue
   *
   * @return
   */
  public function get(string $property, $defaultValue = null)
  {

    /**
     * Check first if the bag is initialized
     */
    if ($this->initialized === false)
    {
      $this->initialize();
    }

    /**
     * Retrieve the data
     */
    if ($this->data[$property])
    {
      return $this->data[$property];
    }

    return $defaultValue;
  }

  /**
   * Sets a value in the session bag
   *<code>
   * $user->set('name', 'Kimbra');
   *</code>
   *
   * @param string $property
   * @param        $value
   */
  public function set(string $property, $value)
  {
    if ($this->initialized === false)
    {
      $this->initialize();
    }

    $this->data[$property] = $value;
    $this->session->set($this->name, $this->data);
  }

  /**
   * Magic isset to check whether a property is defined in the bag
   *<code>
   * var_dump(isset($user['name']));
   *</code>
   *
   * @param string $property
   *
   * @return bool
   */
  public function __isset(string $property): bool
  {
    return $this->has($property);
  }

  /**
   * Check whether a property is defined in the internal bag
   *<code>
   * var_dump($user->has('name'));
   *</code>
   *
   * @param string $property
   *
   * @return bool
   */
  public function has(string $property): bool
  {
    if ($this->initialized === false)
    {
      $this->initialize();
    }

    return isset ($this->data[$property]);
  }

  /**
   * Magic unset to remove items using the array syntax
   *<code>
   * unset($user['name']);
   *</code>
   *
   * @param string $property
   *
   * @return bool
   */
  public function __unset(string $property): bool
  {
    return $this->remove($property);
  }

  /**
   * Removes a property from the internal bag
   *<code>
   * $user->remove('name');
   *</code>
   *
   * @param string $property
   *
   * @return bool
   */
  public function remove(string $property): bool
  {

    if (isset ($this->data[$property]))
    {
      unset ($this->data[$property]);

      return true;
    }

    return false;
  }

  /**
   * Return length of bag
   *<code>
   * echo $user->count();
   *</code>
   */
  public final function count(): int
  {
    if ($this->initialized === false)
    {
      $this->initialize();
    }

    return count($this->data);
  }

  /**
   * Returns the bag iterator
   */
  public final function getIterator(): ArrayIterator
  {
    if ($this->initialized === false)
    {
      $this->initialize();
    }

    return new \ArrayIterator($this->data);
  }

  public final function offsetSet($property, $value)
  {
    return $this->set($property, $value);
  }

  public final function offsetExists($property): bool
  {
    return $this->has($property);
  }

  public final function offsetUnset($property)
  {
    return $this->remove($property);
  }

  public final function offsetGet($property)
  {
    return $this->get($property);
  }
}
