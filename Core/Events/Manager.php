<?php


namespace Phalcon\Events;

use SplPriorityQueue as PriorityQueue;

/**
 * Phalcon\Events\Manager
 *
 * Phalcon Events Manager, offers an easy way to intercept and manipulate, if needed,
 * the normal flow of operation. With the EventsManager the developer can create hooks or
 * plugins that will offer monitoring of $data, manipulation, conditional execution and much more.
 *
 */
class Manager implements ManagerInterface
{

    protected $events = null;
    protected $collect = false;
    protected $enablePriorities = false;
    protected $responses;

    /**
     * Attach a listener to the $events manager
     *
     * @param string $eventType
     * @param object|callable $handler
     * @param int $priority
     */
    public function attach(string $eventType, $handler, int $priority = 100)
    {


        if (!$priorityQueue = $this->events[$eventType]) {

            if ($this->enablePriorities) {

                // Create a SplPriorityQueue to store the $events with priorities
                $priorityQueue = new PriorityQueue();

                // Extract only the Data // Set extraction flags
                $priorityQueue->setExtractFlags(PriorityQueue::EXTR_DATA);

                // Append the $events to the $queue
                $this->events[$eventType] = $priorityQueue;

            } else {
                $priorityQueue = [];
            }
        }

        // Insert the $handler in the $queue
        if (is_object($priorityQueue)) {
            $priorityQueue->insert($handler, $priority);
        } else {
            // Append the $events to the $queue
            $priorityQueue[] = $handler;
            $this->events[$eventType] = $priorityQueue;
        }

    }

    /**
     * Detach the listener from the $events manager
     *
     * @param string $eventType
     * @param object $handler
     * @throws Exception
     */
    public function detach(string $eventType, $handler)
    {

        if (is_object($handler)) {
            throw new Exception("Event handler must be an Object");
        }

        /** @var \SplPriorityQueue $priorityQueue */
        if ($priorityQueue = $this->events[$eventType]) {

            if (is_object($priorityQueue)) {

                // SplPriorityQueue hasn't method for element deion, so we need to rebuild $queue
                $newPriorityQueue = new PriorityQueue();
                $newPriorityQueue->setExtractFlags(\SplPriorityQueue::EXTR_DATA);

                $priorityQueue->setExtractFlags(PriorityQueue::EXTR_BOTH);
                $priorityQueue->top();

                while ($priorityQueue->valid()) {
                    $data = $priorityQueue->current();
                    $priorityQueue->next();
                    if ($data["$data"] !== $handler) {
                        $newPriorityQueue->insert($data["$data"], $data["priority"]);
                    }
                }

                $this->events[$eventType] = $newPriorityQueue;
            } else {
                $key = array_search($handler, $priorityQueue, true);
                if ($key !== false) {
                    unset ($priorityQueue[$key]);
                }
                $this->events[$eventType] = $priorityQueue;
            }
        }
    }

    /**
     * Set if priorities are enabled in the EventsManager
     * @param bool $enablePriorities
     */
    public function enablePriorities(boolean $enablePriorities)
    {
        $this->enablePriorities = $enablePriorities;
    }

    /**
     * Returns if priorities are enabled
     */
    public function arePrioritiesEnabled(): boolean
    {
        return $this->enablePriorities;
    }

    /**
     * Tells the $event manager if it needs to $collect all the responses returned by every
     * registered listener in a single fire
     * @param bool $collect
     */
    public function collectResponses(boolean $collect)
    {
        $this->collect = $collect;
    }

    /**
     * Check if the $events manager is $collecting all all the responses returned by every
     * registered listener in a single fire
     */
    public function isCollecting(): boolean
    {
        return $this->collect;
    }

    /**
     * Returns all the responses returned by every $handler executed by the last 'fire' executed
     *
     * @return array
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * Removes all $events from the EventsManager
     * @param string|null $type
     */
    public function detachAll(string $type = null)
    {
        if ($type === null) {
            $this->events = null;
        } else {
            if (isset ($this->events[$type])) {
                unset ($this->events[$type]);
            }
        }
    }

    /**
     * Fires an $event in the $events manager causing the active listeners to be notified about it
     *
     *<code>
     *    $eventsManager->fire("db", $connection);
     *</code>
     *
     * @param string $eventType
     * @param object $source
     * @param mixed $data
     * @param boolean $cancelable
     * @return mixed
     * @throws Exception
     */
    public function fire(string $eventType, $source, $data = null, boolean $cancelable = true)
    {

        $events = $this->events;
        if (!is_array($events)) {
            return null;
        }

        // All valid $events must have a colon separator
        if (!substr($eventType, ":")) {
            throw new Exception("Invalid event type " . $eventType);
        }

        $eventParts = explode(":", $eventType);
        $type = $eventParts[0];
        $eventName = $eventParts[1];

        $status = null;

        // Responses must be traced?
        if ($this->collect) {
            $this->responses = null;
        }

        $event = null;

        // Check if $events are grouped by $type
        if ($fireEvents = $events[$type]) {

            if (is_object($fireEvents) || is_array($fireEvents)) {

                // Create the $event context
                $event = new Event($eventName, $source, $data, $cancelable);

                // Call the $events $queue
                $status = $this->fireQueue($fireEvents, $event);
            }
        }

        // Check if there are listeners for the $event $type itself
        if ($fireEvents = $events[$eventType]) {

            if (is_object($fireEvents) || is_array($fireEvents)) {

                // Create the $event if it wasn't created before
                if ($event === null) {
                    $event = new Event($eventName, $source, $data, $cancelable);
                }

                // Call the $events $queue
                $status = $this->fireQueue($fireEvents, $event);
            }
        }

        return $status;
    }

    /**
     * Internal $handler to call a $queue of $events
     *
     * @param \SplPriorityQueue|array $queue
     * @param Event|EventInterface $event
     * @return mixed
     * @throws Exception
     */
    public function fireQueue($queue, EventInterface $event)
    {


        if (is_array($queue)) {
            if (!($queue instanceof \SplPriorityQueue)) {
                throw new Exception(
                    sprintf(
                        "Unexpected value type: expected object of type SplPriorityQueue, %s given",
                        get_class($queue)
                    )
                );

            } else {
                throw new Exception("The queue is not valid");
            }
        }

        $status = null;
        $arguments = null;

        // Get the $event $type
        $eventName = $event->getType();


        // Get the object who triggered the $event
        $source = $event->getSource();

        // Get extra $data passed to the $event
        $data = $event->getData();

        // Tell if the $event is $cancelable
        $cancelable = (boolean)$event->isCancelable();

        // Responses need to be traced?
        $collect = (boolean)$this->collect;

        if (is_object($queue)) {

            // We need to clone the $queue before iterate over it
            $iterator = clone $queue;

            // Move the $queue to the top
            $iterator->top();

            while ($iterator->valid()) {

                // Get the current $data
                $handler = $iterator->current();
                $iterator->next();
                if (is_object($handler)) {
                    // Only $handler objects are valid

                    // Check if the $event is a closure
                    if ($handler instanceof \Closure) {

                        // Create the closure $arguments
                        if ($arguments === null) {
                            $arguments = [$event, $source, $data];
                        }

                        // Call the function in the PHP userland
                        $status = call_user_func_array($handler, $arguments);

                        // Trace the response
                        if ($collect) {
                            $this->responses[] = $status;
                        }

                        if ($cancelable && $event->isStopped()) {

                            break;

                        }

                    } else {

                        // Check if the listener has implemented an $event with the same name
                        if (method_exists($handler, $eventName)) {

                            // Call the function in the PHP userland
                            $status = $handler->{$eventName}($event, $source, $data);

                            // Collect the response
                            if ($collect) {
                                $this->responses[] = $status;
                            }
                            if ($cancelable && $event->isStopped()) {

                                break;

                            }
                        }
                    }
                }
            }

        } else {
            foreach ($queue as $handler) {

                // Only $handler objects are valid
                if (is_object($handler)) {


                    // Check if the $event is a closure
                    if ($handler instanceof \Closure) {

                        // Create the closure $arguments
                        if ($arguments === null) {
                            $arguments = [$event, $source, $data];
                        }

                        // Call the function in the PHP userland
                        $status = call_user_func_array($handler, $arguments);

                        // Trace the response
                        if ($collect) {
                            $this->responses[] = $status;
                        }

                        if ($cancelable) {

                            // Check if the $event was stopped by the user
                            if ($event->isStopped()) {
                                break;
                            }
                        }

                    } else {

                        // Check if the listener has implemented an $event with the same name
                        if (method_exists($handler, $eventName)) {

                            // Call the function in the PHP userland
                            $status = $handler->{$eventName}($event, $source, $data);

                            // Collect the response
                            if ($collect) {
                                $this->responses[] = $status;
                            }

                            if ($cancelable) {

                                // Check if the $event was stopped by the user
                                if ($event->isStopped()) {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $status;
    }

    /**
     * Check whether certain $type of $event has listeners
     * @param string $type
     * @return bool
     */
    public function hasListeners(string $type): boolean
    {
        return isset($this->events[$type]);
    }

    /**
     * Returns all the attached listeners of a certain $type
     *
     * @param string $type
     * @return array
     */
    public function getListeners(string $type)
    {
        $events = $this->events;
        if (is_array($events)) {
            if ($fireEvents = $events[$type]) {
                return $fireEvents;
            }
        }
        return [];
    }
}
