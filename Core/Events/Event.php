<?php

namespace Phalcon\Events;
use Exception;

/**
 * Phalcon\Events\Event
 *
 * This class offers contextual information of a fired event in the EventsManager
 */
class Event implements EventInterface
{
	/**
	 * Event type
	 *
	 * @var string
	 */
	protected $type;

	/**
	 * Event source
	 *
	 * @var object
	 */
	protected $source;

	/**
	 * Event data
	 *
	 * @var mixed
	 */
	protected $data;

	/**
	 * Is event propagation stopped?
	 *
	 * @var boolean
	 */
	protected $stopped = false;

	/**
	 * Is event cancelable?
	 *
	 * @var boolean
	 */
	protected $cancelable = true;

	/**
	 * Phalcon\Events\Event constructor
	 *
	 * @param string $type
	 * @param object $source
	 * @param mixed $data
	 * @param boolean $cancelable
	 */
	public function __construct(string $type, $source, $data = null, boolean $cancelable = true)
	{
		 $this->type = $type;
			$this->source = $source;

		if ($data !== null) {
			 $this->data = $data;
		}

		if ($cancelable !== true ){
			 $this->cancelable = $cancelable;
		}
	}

    /**
     * Sets event data.
     * @param mixed $data
     * @return EventInterface
     */
	public function setData($data = null): EventInterface
	{
		 $this->data = $data;

		return $this;
	}

    /**
     * Sets event type.
     * @param string $type
     * @return EventInterface
     */
	public function setType(string $type): EventInterface
	{
		 $this->type = $type;

		return $this;
	}

	/**
	 * Stops the event preventing propagation.
	 *
	 * <code>
	 * if ($event->isCancelable()) {
	 *     $event->stop();
	 * }
	 * </code>
	 */
	public function stop(): EventInterface
	{
		if (!$this->cancelable) {
			throw new Exception("Trying to cancel a non-cancelable event");
		}

		 $this->stopped = true;

		return $this;
	}

	/**
	 * Check whether the event is currently stopped.
	 */
	public function isStopped():boolean
	{
		return $this->stopped;
	}

	/**
	 * Check whether the event is cancelable.
	 *
	 * <code>
	 * if ($event->isCancelable()) {
	 *     $event->stop();
	 * }
	 * </code>
	 */
	public function isCancelable():boolean
	{
		return $this->cancelable;
	}

    /**
     * Gets event $data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Gets event type
     */
    public function getType()
    {
        return   $this->type;
    }
}
