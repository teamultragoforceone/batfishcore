<?php


namespace Phalcon\Events;

/**
 * Phalcon\Events\EventInterface
 *
 * Interface for Phalcon\Events\Event class
 */
interface EventInterface
{
	/**
	 * Gets event $data
	 */
	public function getData();

    /**
     * Sets event $data
     * @param mixed $data
     * @return EventInterface
     */
	public function setData($data = null):EventInterface;

	/**
	 * Gets event type
	 */
	public function getType();

    /**
     * Sets event type
     * @param string $type
     * @return EventInterface
     */
	public function setType(string $type):EventInterface;

	/**
	 * Stops the event preventing propagation
	 */
	public function stop():EventInterface;

	/**
	 * Check whether the event is currently stopped
	 */
	public function isStopped():boolean;

	/**
	 * Check whether the event is cancelable
	 */
	public function isCancelable():boolean;
}
