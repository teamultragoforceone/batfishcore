<?php

namespace BatFish\Core\Collection;

use Exception;
use JsonSerializable;
use stdClass;

abstract class AbstractEntity extends stdClass  implements JsonSerializable
{
    public $guid;

  /**
   * Specify data which should be serialized to JSON
   *
   * @link  http://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   *        which is a value of any type other than a resource.
   * @since 5.4.0
   */
  public function jsonSerialize()
  {
    return get_object_vars($this);
  }
    /**
     * @return mixed
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param mixed $guid
     * @return $this
     */
    public function setGuid(string $guid)
    {
        $this->guid = $guid;
        return $this;
    }


    public function __get($name)
    {
        if(isset($this->field[$name])){
            return $this->field[$name];
        }

        else{
            throw new Exception("$name dow not exists");
        }

    }

    public function getObjectData($name){

       $vars = get_object_vars($this);


        if(isset($vars[$name])){
            return $vars[$name];
        }

        else{
            throw new Exception("$name dow not exists");
        }

    }
}