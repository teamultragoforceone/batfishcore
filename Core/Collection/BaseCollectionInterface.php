<?php

namespace BatFish\Core\Collection;

interface BaseCollectionInterface
{

  /**
   * BaseCollectionInterface constructor.
   * @param array $items
   */
  public function __construct(array $items = []);

  /*
   * The below methods are for copy-paste use, put them
   * in your custom EntityCollectionInterface, modify the type hinting
   * and then implement them on your EntityCollection
   */

  //  /**
  //   * @param          $key
  //   * @param Entity   $value
  //   *
  //   * @return static
  //   */
  //  public function set($key, Entity $value)
  //  {
  //    return $this->internalOffsetSet($key, $value);
  //  }

  //  /**
  //   * Merge a collection into this
  //   *
  //   * @param ThisCollectionInterface   $collection
  //   *
  //   * @return static
  //   */
  //  public function merge(ThisCollectionInterface $collection)
  //  {
  //    return $this->internalMerge($collection);
  //  }

  /*
   * The below methods are implemented on the AbstractCollection object.
   * Simply extend the AbstractCollection to make use of them.
   */
  /**
   * Shuffle the items in the collection.
   *
   * @return static
   */
  public function shuffle();

  /**
   * Count the items in the collection.
   *
   * @return int
   */
  public function count();

  /**
   * Get all items
   *
   * @return array
   */
  public function getAll();

  /**
   * Offset to unset
   *
   * @param int $key
   *
   * @return static
   */
  public function remove($key);

  /**
   * Get the first item from the collection.
   *
   * @return AbstractEntity|null
   */
  public function first();

  /**
   * Get the last item from the collection.
   *
   * @return AbstractEntity|null
   */
  public function last();

  /**
   * Get an item at a given offset.
   *
   * @param  int|string $key
   *
   * @return mixed|null
   */
  public function get($key);

  /**
   * Determine if an item exists at an offset.
   *
   * @param  mixed $key
   *
   * @return bool
   */
  public function exists($key);

  /**
   * Filters the items in a collection using a callback and returns a new
   * collection containing the items for which the callback returned true.
   *
   * The callback takes two arguments, the item's key and the item itself.
   *
   * @param  callable $filterCallback The filter callback.
   *
   * @return static
   */
  public function filter(callable $filterCallback);

  /**
   * Maps the items in a collection using a callback and returns a new
   * collection containing the values returned by the callback.
   *
   * The callback takes two arguments, the item's key and the item itself.
   *
   * Returns an UntypedCollection by default, but a specific collection type can be
   * passed (as instance, or fully qualified class name) as the second parameter.
   *
   * @param  callable $mapCallback The filter callback.
   * @param string|AbstractCollection If provided, the returned collection is of this type.
   *
   * @return AbstractCollection
   */
  public function map(callable $mapCallback, $collectionType = null);

  /**
   * @param mixed $item
   *
   * @return AbstractCollection
   */
  public function add($item);

  /**
   * @param callable $callback
   *
   * @return AbstractCollection
   */
  public function sort(callable $callback);
}
