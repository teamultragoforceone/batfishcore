<?php

namespace BatFish\Core\Collection;

class UntypedCollection extends AbstractCollection implements BaseCollectionInterface
{
  /**
   * @param scalar $key
   * @param mixed $value
   *
   * @return static
   */
  public function set($key, $value)
  {
    return $this->internalOffsetSet($key, $value);
  }

  /**
   * Merge a collection into this
   *
   * @param BaseCollectionInterface $collection
   *
   * @return static
   */
  public function merge(BaseCollectionInterface $collection)
  {
    return $this->internalMerge($collection);
  }

  /**
   * @param scalar $key
   * @param mixed $value
   *
   * @return static
   */
  public function offsetSet($key, $value)
  {
    return $this->internalOffsetSet($key, $value);
  }
}
