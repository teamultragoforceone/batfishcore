<?php

namespace BatFish\Core\Collection;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use JsonSerializable;

/**
 * AbstractCollection
 */
class AbstractCollection
    implements
    BaseCollectionInterface,
    Countable,
    IteratorAggregate,
    JsonSerializable
{
    /**
     * The items contained in the collection.
     *
     * @var array
     */
    protected $items = [];

    public function __construct(array $items = [])
    {
        // Fixes IDE errors, and leaves your concrete collection requiring offsetSet
        if (method_exists($this, 'offsetSet')) {
            foreach ($items as $key => $value) {
                $this->offsetSet($key, $value);
            }
        } else {
            throw new \Exception(
                "offsetSet must be implemented in your concrete collection"
            );
        }
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     *
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     *       which is a value of any type other than a resource.
     */
    function jsonSerialize()
    {
        $retun = [];
        foreach ($this->items as $item) {
            if ($item instanceof JsonSerializable) {
                $retun[] = $item->jsonSerialize();
            }
            $retun[] = $item ;


        }
        return $retun;
    }

    /**
     * Get all items.
     *
     * @return array
     */
    public function getAll()
    {
        return $this->items;
    }

    /**
     * Get an item at a given offset.
     *
     * @param  int|string $key
     *
     * @return mixed|null
     */
    public function get($key)
    {
        return $this->items[$key];
    }

    /**
     * Shuffle the items in the collection.
     *
     * @return $this
     */
    public function shuffle()
    {
        shuffle($this->items);

        return $this;
    }

    /**
     * Count the items in the collection.
     *
     * @return int
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     * Determine if an item exists at an offset.
     *
     * @param  mixed $key
     *
     * @return bool
     */
    public function exists($key)
    {
        return array_key_exists($key, $this->items);
    }

    /**
     * Unset the item at a given offset.
     *
     * @param  string $key
     *
     * @return static
     */
    public function remove($key)
    {
        unset($this->items[$key]);

        return $this;
    }

    /**
     * Get the first item from the collection.
     *
     * @return AbstractEntity|null
     */
    public function first()
    {
        foreach ($this->items as $item) {
            return $item;
        }

        return null;
    }

    /**
     * Get the last item from the collection.
     *
     * @return AbstractEntity|null
     */
    public function last()
    {
        foreach (array_reverse($this->items) as $item) {
            return $item;
        }

        return null;
    }

    /**
     * Sort the collection by a callback;
     *
     * @param callable $callback
     *
     * @return $this
     */
    public function sort(callable $callback)
    {
        usort($this->items, $callback);

        return $this;
    }

    /**
     * Returns a new collection containing a slice of the current.
     *
     * @param          $start
     * @param int|null $length
     *
     * @return static
     */
    public function slice($start, $length = null)
    {
        return new static(array_slice($this->items, $start, $length));
    }

    /**
     * Filters the items in a collection using a callback and returns a new
     * collection containing the items for which the callback returned true.
     * The callback takes two arguments, the item's key and the item itself.
     *
     * @param  callable $filterCallback The filter callback.
     *
     * @return static
     */
    public function filter(callable $filterCallback)
    {
        $filteredCollection = new static;

        foreach ($this->getIterator() as $key => $item) {
            if (call_user_func($filterCallback, $key, $item)) {
                $filteredCollection->internalOffsetSet($key, $item);
            }
        }

        return $filteredCollection;
    }

    /**
     * Get an iterator for the collection.
     *
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }

    /**
     * Set the item at a given offset.
     *
     * @param  mixed $key
     * @param  mixed $value
     *
     * @return static
     */
    protected function internalOffsetSet($key, $value)
    {
        $this->items[$key] = $value;

        return $this;
    }

    public function map(callable $mapCallback, $collectionType = null)
    {
        if ($collectionType != null) {
            $mappedCollection = new $collectionType;
        } else {
            $mappedCollection = new UntypedCollection;
        }

        foreach ($this->getIterator() as $key => $item) {
            $newItem = call_user_func($mapCallback, $key, $item);
            $mappedCollection->internalOffsetSet($key, $newItem);
        }

        return $mappedCollection;
    }

    /**
     * @param mixed $item
     *
     * @return $this
     */
    public function add($item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Merge the items into the collection.
     *
     * @param BaseCollectionInterface $collection
     *
     * @return static
     * @throws \Exception
     */
    protected function internalMerge(BaseCollectionInterface $collection)
    {
        if (method_exists($this, 'offsetSet')) {
            foreach ($collection->getAll() as $key => $value) {
                $this->offsetSet($key, $value);
            }
        } else {
            throw new \Exception(
                "offsetSet must be implemented in your concrete collection"
            );
        }

        return $this;
    }
}
