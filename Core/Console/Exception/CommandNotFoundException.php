<?php
namespace BatFish\Core\Console\Exception;

use Exception;
use BatFish\Core\Console\CommandBase;
use BatFish\Core\Console\Exception\CommandBaseException;

class CommandNotFoundException extends CommandBaseException
{
    public $name;

    public function __construct(CommandBase $command, $name)
    {
        $this->name = $name;
        parent::__construct($command, "Command $name not found.");
    }
}
