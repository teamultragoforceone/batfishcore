<?php
namespace BatFish\Core\Console\Exception;

use Exception;
use BatFish\Core\Console\Exception\CommandBaseException;
use BatFish\Core\Console\CommandBase;

class InvalidCommandArgumentException extends CommandBaseException
{
    public $arg;

    public $argIndex;

    public function __construct(CommandBase $command, $argIndex, $arg)
    {
        $this->argIndex = $argIndex;
        $this->arg = $arg;
        parent::__construct($command, "Invalid '{$command->getName()}' command argument '$arg' at position $argIndex");
    }
}
