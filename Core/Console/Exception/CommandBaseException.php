<?php
namespace BatFish\Core\Console\Exception;

use Exception;
use BatFish\Core\Console\CommandBase;

class CommandBaseException extends Exception
{
    public $command;

    public function __construct(CommandBase $command, $message = "", $code = 0, $previous = null)
    {
        $this->command = $command;
        parent::__construct($message, $code, $previous);
    }

    public function getCommand()
    {
        return $this->command;
    }
}
