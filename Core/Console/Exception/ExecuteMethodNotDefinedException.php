<?php
namespace BatFish\Core\Console\Exception;

use Exception;

class ExecuteMethodNotDefinedException extends CommandBaseException
{
}
