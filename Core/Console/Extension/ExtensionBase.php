<?php
namespace BatFish\Core\Console\Extension;

use BatFish\Core\Console\ServiceContainer;
use BatFish\Core\Console\Command;
use BatFish\Core\Console\CommandBase;
use BatFish\Core\Console\Logger;
use GetOptionKit\OptionCollection;
use LogicException;
use BatFish\Core\Console\ArgInfoList;

abstract class ExtensionBase
{
    protected $container;

    public function __construct()
    {
    }

    public function setServiceContainer(ServiceContainer $container)
    {
        $this->container = $container;
    }


    /**
     * init method is called when the extension is added to the pool.
     */
    public function init()
    {
    }

    public static function isSupported()
    {
        return true;
    }

    public function isAvailable()
    {
        return true;
    }

    public function options($opts)
    {
    }

    public function arguments($args)
    {
    }

    public function prepare()
    {
    }

    public function execute()
    {
    }

    public function finish()
    {
    }

    public function __get($accessor)
    {
        if (isset($this->container[$accessor])) {
            return $this->container[$accessor];
        }
        throw new LogicException("Undefined accessor '$accessor'");
    }
}
