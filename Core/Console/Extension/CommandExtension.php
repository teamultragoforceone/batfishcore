<?php
namespace BatFish\Core\Console\Extension;

use BatFish\Core\Console\Command;
use BatFish\Core\Console\Extension\Extension;
use BatFish\Core\Console\Extension\ExtensionBase;

abstract class CommandExtension extends ExtensionBase
{
    protected $config;

    protected $command;

    public function bindCommand(Command $command)
    {
        $this->command = $command;
        $this->options($command->getOptionCollection());
        // $this->arguments( );

        $this->config = $command->getApplication()->getGlobalConfig();
        $this->setServiceContainer($command->getApplication()->getService());
        $this->init();
    }
}
