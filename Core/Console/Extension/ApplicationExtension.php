<?php
namespace BatFish\Core\Console\Extension;

use BatFish\Core\Console\Application;

interface ApplicationExtension extends Extension
{
    public function bindApplication(Application $app);
}
