<?php
namespace BatFish\Core\Console\ExceptionPrinter;

use Exception;
use BatFish\Core\Console\ServiceContainer;
use BatFish\Core\Console\Logger;

class ProductionExceptionPrinter extends DevelopmentExceptionPrinter
{
    public $reportUrl;

    public function dump(Exception $e)
    {
        $this->dumpBrief($e);
        $this->dumpTraceInPhar($e);
    }
}
