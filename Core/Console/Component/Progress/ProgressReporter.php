<?php
namespace BatFish\Core\Console\Component\Progress;

interface ProgressReporter
{
    public function update($finishedValue, $totalValue);
}
