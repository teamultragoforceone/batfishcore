<?php
namespace BatFish\Core\Console\Component\Progress;

use Exception;
use BatFish\Core\Console\Formatter;
use BatFish\Core\Console\ConsoleInfo\EnvConsoleInfo;
use BatFish\Core\Console\ConsoleInfo\ConsoleInfoFactory;

class ProgressBarStyle
{
    public $leftDecorator = "|";

    public $rightDecorator = "|";

    public $barCharacter = '#';
}
