<?php
namespace BatFish\Core\Console\Component\Table;

use BatFish\Core\Console\Component\Table\CellAttribute;
use NumberFormatter;

class CurrencyFormatCell extends NumberFormatCell
{
    protected $currency;

    public function __construct($locale, $currency)
    {
        parent::__construct($locale);
        $this->currency = $currency;
    }

    public function format($cell)
    {
        return $this->formatter->formatCurrency($cell, $this->currency);
    }
}
