<?php
namespace BatFish\Core\Console\ConsoleInfo;

use BatFish\Core\Console\ConsoleInfo\EnvConsoleInfo;
use BatFish\Core\Console\ConsoleInfo\TputConsoleInfo;

class ConsoleInfoFactory
{
    public static function create()
    {
        if (EnvConsoleInfo::hasSupport()) {
            return new EnvConsoleInfo;
        } elseif (TputConsoleInfo::hasSupport()) {
            return new TputConsoleInfo;
        }
    }
}
