<?php
namespace BatFish\Core\Console\ConsoleInfo;

interface ConsoleInfoInterface
{
    public function getColumns();
    public function getRows();
    public static function hasSupport();
}
