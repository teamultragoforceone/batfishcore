<?php
namespace BatFish\Core\Console\Debug;

use BatFish\Core\Console\Component\Table\Table;
use BatFish\Core\Console\Component\Table\TableStyle;
use BatFish\Core\Console\Component\Table\CompactTableStyle;
use BatFish\Core\Console\Component\Table\MarkdownTableStyle;
use BatFish\Core\Console\Component\Table\CellAttribute;
use BatFish\Core\Console\Component\Table\NumberFormatCell;
use BatFish\Core\Console\Component\Table\CurrencyCellAttribute;
use BatFish\Core\Console\Component\Table\SpellOutNumberFormatCell;
use BatFish\Core\Console\Component\Table\RowSeparator;
use LazyRecord\BaseCollection;
use Exception;

class ConsoleDebug
{
    public static function dumpException(Exception $e)
    {
        $indicator = new LineIndicator;
        $output = [];
        $output[] = '[' . get_class($e) . '] was thrown with "' . $e->getMessage() . '".';
        $output[] = $indicator->indicateFile($e->getFile(), $e->getLine());

        $output[] = "Exception Stack Trace";
        $output[] = "=====================";
        $output[] = "";
        $output[] = $e->getTraceAsString();
        return join(PHP_EOL, $output);
    }


    /**
     * Dump Record Collection
     */
    public static function dumpCollection(BaseCollection $collection, array $options = array())
    {
        return self::dumpRows($collection->toArray(), $options);
    }


    public static function dumpRows(array $array, array $options = array())
    {
        $table = new Table;

        $keys = null;
        if (isset($options['keys'])) {
            $keys = $options['keys'];
        } elseif (isset($array[0])) {
            $keys = array_keys($array[0]);
        }

        if ($keys) {
            $table->setHeaders($keys);
        }

        if (empty($array)) {
            return '0 rows.' . PHP_EOL;
        }

        foreach ($array as $item) {
            $values = [];
            foreach ($keys as $key) {
                $values[] = $item[$key];
            }
            $table->addRow($values);
        }
        return $table->render() . PHP_EOL
            . count($array) . ' rows.' . PHP_EOL;
    }
}
