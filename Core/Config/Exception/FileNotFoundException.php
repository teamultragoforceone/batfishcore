<?php

namespace BatFish\Core\Config\Exception;

use BatFish\Core\Config\Exception;

class FileNotFoundException extends Exception
{
}
