<?php

namespace BatFish\Core\Config\Exception;

use BatFish\Core\Config\Exception;

class EmptyDirectoryException extends Exception
{
}
