<?php

namespace BatFish\Core\Flash;


/**
 * Phalcon\FlashInterface
 * Interface for Phalcon\Flash
 */
interface FlashInterface
{

  /**
   * Shows a HTML error message
   *
   * @param $message
   *
   * @return
   */
  public function error($message);

  /**
   * Shows a HTML notice/information message
   *
   * @param $message
   *
   * @return
   */
  public function notice($message);

  /**
   * Shows a HTML success message
   *
   * @param $message
   *
   * @return
   */
  public function success($message);

  /**
   * Shows a HTML warning message
   *
   * @param $message
   *
   * @return
   */
  public function warning($message);

  /**
   * Outputs a message
   *
   * @param string $type
   * @param        $message
   *
   * @return
   */
  public function message(string $type, $message);

}