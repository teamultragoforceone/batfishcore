<?php
namespace BatFish\Core\Flash;
use BatFish\Core\Domain\DomainAwareTrait;
use BatFish\Core\Domain\DomainInterface;

/**
 * BatFish\Core\Flash
 * Shows HTML notifications related to different circumstances. Classes can be
 * stylized using CSS
 *<code>
 * $flash->success("The record was successfully deleted");
 * $flash->error("Cannot open the file");
 *</code>
 */
abstract class Flash implements  FlashInterface
{

  protected $cssClasses;

  protected $implicitFlush = true;

  protected $automaticHtml = true;

  protected $autoescape = false;

  protected $dependencyInjector = null;

  protected $messages = [];

  /**
   * BatFish\Core\Flash constructor
   *
   * @param array $cssClasses
   */
  public function __construct($cssClasses = null)
  {
    if (!$cssClasses)
    {
      $cssClasses = [
        'error'   => 'alert alert-danger',
        'notice'  => 'alert alert-info',
        'success' => 'alert alert-success',
        'warning' => 'alert alert-warning',
      ];
    }
    $this->cssClasses = $cssClasses;
  }

  /**
   * Returns the autoescape mode in generated html
   */
  public function getAutoescape()
  {
    return $this->autoescape;
  }

  /**
   * Set the autoescape mode in generated html
   *
   * @param bool $autoescape
   *
   * @return FlashInterface
   */
  public function setAutoescape(boolean $autoescape): FlashInterface
  {
    $this->autoescape = $autoescape;

    return $this;
  }

  /**
   * Set whether the output must be implicitly flushed to the output or
   * returned as string
   *
   * @param bool $implicitFlush
   *
   * @return FlashInterface
   */
  public function setImplicitFlush(boolean $implicitFlush): FlashInterface
  {
    $this->implicitFlush = $implicitFlush;

    return $this;
  }

  /**
   * Set if the output must be implicitly formatted with HTML
   *
   * @param bool $automaticHtml
   *
   * @return FlashInterface
   */
  public function setAutomaticHtml(boolean $automaticHtml): FlashInterface
  {
    $this->automaticHtml = $automaticHtml;

    return $this;
  }

  /**
   * Set an array with CSS classes to format the $messages
   *
   * @param array $cssClasses
   *
   * @return FlashInterface
   */
  public function setCssClasses(array $cssClasses): FlashInterface
  {
    $this->cssClasses = $cssClasses;

    return $this;
  }

  /**
   * Shows a HTML error $message
   *<code>
   * $flash->error("This is an error");
   *</code>
   *
   * @param $message
   *
   * @return
   */
  public function error($message)
  {
    return $this->messages['error'][] = $message;
  }

  /**
   * Shows a HTML notice/information $message
   *<code>
   * $flash->notice("This is an information");
   *</code>
   *
   * @param $message
   *
   * @return
   */
  public function notice($message)
  {
    return $this->messages['notice'][] = $message;
  }

  /**
   * Shows a HTML success $message
   *<code>
   * $flash->success("The process was finished successfully");
   *</code>
   *
   * @param $message
   *
   * @return
   */
  public function success($message)
  {
    return $this->messages['success'][] = $message;
  }

  /**
   * Shows a HTML warning $message
   *<code>
   * $flash->warning("Hey, $this is important");
   *</code>
   *
   * @param $message
   *
   * @return
   */
  public function warning($message)
  {
    return $this->messages['warning'][]= $message;
  }

  /**
   * Outputs a $message formatting it with HTML
   *<code>
   * $flash->outputMessage("error", $message);
   *</code>
   *
   * @param string       $type
   * @param string|array $message
   *
   * @return string|void
   */
  public function outputMessage(string $type, $message)
  {

    $automaticHtml = (bool)$this->automaticHtml;
    $cssClasses = '';
    $eol = '';
    $content = '';
    if ($automaticHtml === true)
    {
      $classes = $this->cssClasses;
      if ($classes)
      {
        if (is_array($classes))
        {
          $cssClasses = ' class="' . implode(' ', $classes) . '"';
        }
        else
        {
          $cssClasses = ' class="' . $classes . '"';
        }
      }
      else
      {
        $cssClasses = '';
      }
      $eol = PHP_EOL;
    }

    $implicitFlush = (bool)$this->implicitFlush;
    if ($message)
    {

      /**
       * We create the $message with implicit flush or other
       */
      if ($implicitFlush === false)
      {
        $content = '';
      }

      /**
       * We create the $message with implicit flush or other
       */
      foreach ($message as $msg)
      {

        $preparedMsg = $msg;

        /**
         * We create the applying formatting or not
         */
        $htmlMessage = $preparedMsg;
        if ($automaticHtml === true)
        {
          $htmlMessage = "<div $cssClasses > $preparedMsg  </div>  $eol";
        }
        if ($implicitFlush === true)
        {
          echo $htmlMessage;
        }
        else
        {
          $content .= $htmlMessage;
          $this->messages[] = $htmlMessage;
        }
      }

      /**
       * We return the $message as string if the $implicitFlush is turned off
       */
      if ($implicitFlush === false)
      {
        return $content;
      }
    }
    else
    {

      $preparedMsg = $message;

      /**
       * We create the applying formatting or not
       */
      if ($automaticHtml === true)
      {
        $htmlMessage = "<div $cssClasses > $preparedMsg </div> $eol";
      }
      else
      {
        $htmlMessage = $preparedMsg;
      }

      /**
       * We return the $message as string if the implicit_flush is turned off
       */
      if ($implicitFlush === true)
      {
        echo $htmlMessage;
      }
      else
      {
        $this->messages[] = $htmlMessage;

        return $htmlMessage;
      }
    }
  }

  /**
   * Clears accumulated $messages when implicit flush is disabled
   */
  public function clear()
  {
    $this->messages = [];
  }

  /**
   * @return mixed
   */
  public function getMessages()
  {
    return $this->messages;
  }


}
