<?php

namespace BatFish\Core\Flash;


use BatFish\Core\Domain\DomainAwareTrait;
use BatFish\Core\Domain\DomainInterface;

class Session extends Flash implements DomainInterface
{
    use DomainAwareTrait;

    public function error($message)
    {

        $return =  parent::error($message);
        $this->save();
        return $return;

    }

    public function notice($message)
    {
        $return =  parent::notice($message);
        $this->save();
        return $return;

    }

    public function success($message)
    {
        $return =  parent::success($message);
        $this->save();
        return $return;

    }

    public function warning($message)
    {
        $return =  parent::warning($message);
        $this->save();
        return $return;

    }

    public function clear()
    {
        $return =  parent::clear();
        $this->getDomain()->getSession()->remove('flash');
        return $return;
    }

    /**
     * Outputs a message
     *
     * @param string $type
     * @param        $message
     *
     * @return string|void
     */
    public function message(string $type, $message)
    {
        return $this->outputMessage($type, $message);
    }

    /**
     * Prints the messages accumulated in the flasher
     *
     * @param bool $remove
     */
    public function output($remove = true)
    {
        $this->messages =  $this->getDomain()->getSession()->get('flash', []);
        $messages = $this->messages;
        if ($messages) {
            foreach ($messages as $type => $message) {
                $this->outputMessage($type, $message);
            }
        }

        if ($remove) {
            parent::clear();

        }
    }

    protected function save()
    {
        $this->getDomain()->getSession()->set('flash', $this->messages);

    }
}
