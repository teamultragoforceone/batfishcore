<?php
namespace BatFish\Core\Flash;


class Direct extends Flash
{

  /**
   * Outputs a message
   *
   * @param string $type
   * @param        $message
   *
   * @return string|void
   */
  public function message(string $type, $message)
  {
    return $this->outputMessage($type, $message);
  }

  /**
   * Prints the messages accumulated in the flasher
   *
   * @param bool $remove
   */
  public function output($remove = true)
  {

    $messages = $this->messages;
    if ($messages)
    {
      foreach ($messages as $type =>$message)
      {
         $this->outputMessage($type, $message);
      }
    }

    if ($remove)
    {
      parent::clear();
    }
  }
}
