<?php
/**
 * Created by IntelliJ IDEA.
 * User: kev
 * Date: 26/11/2017
 * Time: 23:33
 */

namespace BatFish\Core\Middleware;


use BatFish\Core\Domain\Component;

abstract class AbstractMiddleware extends Component
{
    abstract function run();

}