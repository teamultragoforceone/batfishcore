<?php

namespace BatFish\Core\Cache\Frontend;

/**
 * Class Json
 *
 * @package BatFish\Core\Cache\Frontend
 *
 * Allows to cache data converting/deconverting them to JSON.
 *
 * This adapter uses the json_encode/json_decode PHP's functions
 *
 * As the data is encoded in JSON other systems accessing the same backend could
 * process them
 *
 *<code>
 *<?php
 *
 * // Cache the data for 2 days
 * $frontCache = new \BatFish\Core\Cache\Frontend\Json(array(
 *    "lifetime" => 172800
 * ));
 *
 * //Create the Cache setting memcached connection options
 * $cache = new \BatFish\Core\Cache\Backend\Memcache($frontCache, array(
 *        'host' => 'localhost',
 *        'port' => 11211,
 *    'persistent' => false
 * ));
 *
 * //Cache arbitrary data
 * $cache->save('my-data', array(1, 2, 3, 4, 5));
 *
 * //Get data
 * $data = $cache->get('my-data');
 *</code>
 */
class Json extends AbstractFrontend
{
  /**
   * Serializes data before storing them
   *
   * @param mixed $data
   * @return string
   */
  public function beforeStore($data)
  {
    return json_encode($data);
  }

  /**
   * Unserializes data after retrieval
   *
   * @param mixed $data
   * @return mixed
   */
  public function afterRetrieve($data)
  {
    return json_decode($data);
  }
}
