<?php
namespace BatFish\Core\Cache\Frontend;

/**
 * Class Msgpack
 *
 * @package BatFish\Core\Cache\Frontend
 *
 * Allows to cache native PHP data in a serialized form using msgpack extension
 * This adapter uses a Msgpack frontend to store the cached content and requires msgpack extension.
 *
 * @link https://github.com/msgpack/msgpack-php
 *
 *<code>
 * use BatFish\Core\Cache\Backend\File;
 * use BatFish\Core\Cache\Frontend\Msgpack;
 *
 * // Cache the files for 2 days using Msgpack frontend
 * $frontCache = new Msgpack([
 *     'lifetime' => 172800
 * ]);
 *
 * // Create the component that will cache "Msgpack" to a "File" backend
 * // Set the cache file directory - important to keep the "/" at the end of
 * // of the value for the folder
 * $cache = new File($frontCache, [
 *     'cacheDir' => '../app/cache/'
 * ]);
 *
 * // Try to get cached records
 * $cacheKey = 'robots_order_id.cache';
 * $robots   = $cache->get($cacheKey);
 * if ($robots === null) {
 *     // $robots is null due to cache expiration or data do not exist
 *     // Make the database call and populate the variable
 *     $robots = Robots::find(['order' => 'id']);
 *
 *     // Store it in the cache
 *     $cache->save($cacheKey, $robots);
 * }
 *
 * // Use $robots
 * foreach ($robots as $robot) {
 *     echo $robot->name, "\n";
 * }
 *</code>
 */
class Msgpack extends AbstractFrontend
{
  /**
   * BatFish\Core\Cache\Frontend\Base64 constructor
   *
   * @param array $frontendOptions
   *
   * @throws \Exception
   */
  public function __construct($frontendOptions = null)
  {
    if (!function_exists('msgpack_pack')) {
      throw new \Exception("Msgpack package is not installed");
    }
    parent::__construct($frontendOptions);
  }

  /**
   * Serializes data before storing them
   *
   * @param mixed data
   *
   * @return mixed|string
   */
  public function beforeStore($data)
  {
    return msgpack_pack($data);
  }

  /**
   * Unserializes data after retrieval
   *
   * @param mixed data
   *
   * @return mixed
   */
  public function afterRetrieve($data)
  {
    return msgpack_unpack($data);
  }
}
