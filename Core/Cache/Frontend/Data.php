<?php

namespace BatFish\Core\Cache\Frontend;

/**
 * Class Data
 *
 * @package BatFish\Core\Cache\Frontend
 *
 * Allows to cache native PHP data in a serialized form
 *
 *<code>
 *  use BatFish\Core\Cache\Backend\File;
 *  use BatFish\Core\Cache\Frontend\Data;
 *  // Cache the files for 2 days using a Data frontend
 *  $frontCache = new Data(['lifetime' => 172800]);
 *  // Create the component that will cache "Data" to a 'File' backend
 *  // Set the cache file directory - important to keep the '/' at the end of
 *  // of the value for the folder
 *  $cache = new File($frontCache, ['cacheDir' => '../app/cache/']);
 *  // Try to get cached records
 *  $cacheKey = 'robots_order_id.cache';
 *  $robots   = $cache->get($cacheKey);
 *  if ($robots === null) {
 *      // $robots is null due to cache expiration or data does not exist
 *      // Make the database call and populate the variable
 *      $robots = Robots::find(['order' => 'id']);
 *      // Store it in the cache
 *      $cache->save($cacheKey, $robots);
 *  }
 *  // Use $robots :)
 *  foreach ($robots as $robot) {
 *      echo $robot->name, "\n";
 *  }
 *</code>
 */
class Data extends AbstractFrontend
{


  /**
   * Serializes data before storing them
   *
   * @param mixed $data
   *
   * @return mixed|string
   */
  public function beforeStore($data)
  {
    return serialize($data);
  }

  /**
   * Unserializes data after retrieval
   *
   * @param mixed $data
   *
   * @return mixed
   */
  public function afterRetrieve($data)
  {
    return unserialize($data);
  }
}
