<?php
namespace BatFish\Core\Cache\Frontend;

/**
 * Class Igbinary
 *
 * @package BatFish\Core\Cache\Frontend
 *          Allows to cache native PHP data in a serialized form using igbinary
 *          extension
 *          <code>
 *          // Cache the files for 2 days using Igbinary frontend
 *          $frontCache = new \BatFish\Core\Cache\Frontend\Igbinary(array(
 *          "lifetime" => 172800
 *          ));
 *          // Create the component that will cache "Igbinary" to a "File"
 *          backend
 *          // Set the cache file directory - important to keep the "/" at the
 *          end of
 *          // of the value for the folder
 *          $cache = new \BatFish\Core\Cache\Backend\File($frontCache, array(
 *          "cacheDir" => "../app/cache/"
 *          ));
 *          // Try to get cached records
 *          $cacheKey  = 'robots_order_id.cache';
 *          $robots    = $cache->get($cacheKey);
 *          if ($robots === null) {
 *          // $robots is null due to cache expiration or data do not exist
 *          // Make the database call and populate the variable
 *          $robots = Robots::find(array("order" => "id"));
 *          // Store it in the cache
 *          $cache->save($cacheKey, $robots);
 *          }
 *          // Use $robots :)
 *          foreach ($robots as $robot) {
 *          echo $robot->name, "\n";
 *          }
 *          </code>
 */
class Igbinary extends Data
{
  /**
   * BatFish\Core\Cache\Frontend\Base64 constructor
   *
   * @param array $frontendOptions
   *
   * @throws \Exception
   */
  public function __construct($frontendOptions = null)
  {
    if (!function_exists('igbinary_serialize'))
    {
      throw new \Exception("igbinary package is not installed");
    }
    parent::__construct($frontendOptions);
  }

  /**
   * Serializes data before storing them
   *
   * @param mixed data
   *
   * @return string
   */
  public function beforeStore($data)
  {
    return igbinary_serialize($data);
  }

  /**
   * Unserializes data after retrieval
   *
   * @param mixed data
   *
   * @return mixed
   */
  public function afterRetrieve($data)
  {
    return igbinary_unserialize($data);
  }

}
