<?php

namespace BatFish\Core\Cache\Frontend;

/**
 * Class None
 *
 * @package BatFish\Core\Cache\Frontend
 *
 * Discards any kind of frontend data input. This frontend does not have expiration time or any other options
 *
 *<code>
 *<?php
 *
 *    //Create a None Cache
 *    $frontCache = new \BatFish\Core\Cache\Frontend\None();
 *
 *    // Create the component that will cache "Data" to a "Memcached" backend
 *    // Memcached connection settings
 *    $cache = new \BatFish\Core\Cache\Backend\Memcache($frontCache, array(
 *        "host" => "localhost",
 *        "port" => "11211"
 *    ));
 *
 *    // This Frontend always return the data as it's returned by the backend
 *    $cacheKey = 'robots_order_id.cache';
 *    $robots    = $cache->get($cacheKey);
 *    if ($robots === null) {
 *
 *        // This cache doesn't perform any expiration checking, so the data is always expired
 *        // Make the database call and populate the variable
 *        $robots = Robots::find(array("order" => "id"));
 *
 *        $cache->save($cacheKey, $robots);
 *    }
 *
 *    // Use $robots :)
 *    foreach ($robots as $robot) {
 *        echo $robot->name, "\n";
 *    }
 *</code>
 */
class None extends AbstractFrontend
{

  /**
   * Returns the cache lifetime
   */
  public function getLifetime()
  {
    return 1;
  }

  /**
   * Prepare data to be stored
   *
   * @param mixed $data
   *
   * @return mixed
   */
  public function beforeStore($data)
  {
    return $data;
  }

  /**
   * Prepares data to be retrieved to user
   *
   * @param mixed $data
   *
   * @return mixed
   */
  public function afterRetrieve($data)
  {
    return $data;
  }
}
