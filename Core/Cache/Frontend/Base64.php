<?php

namespace BatFish\Core\Cache\Frontend;

/**
 * Class Base64
 *
 * @package BatFish\Core\Cache\Frontend
 *
 * Allows to cache data converting/deconverting them to base64.
 * This adapter uses the base64_encode/base64_decode PHP's functions
 *
 *<code>
 *<?php
 * // Cache the files for 2 days using a Base64 frontend
 * $frontCache = new \BatFish\Core\Cache\Frontend\Base64(array(
 *    "lifetime" => 172800
 * ));
 * //Create a MongoDB cache
 * $cache = new \BatFish\Core\Cache\Backend\Mongo($frontCache, array(
 *    'server' => "mongodb://localhost",
 *      'db' => 'caches',
 *    'collection' => 'images'
 * ));
 * // Try to get cached image
 * $cacheKey = 'some-image.jpg.cache';
 * $image    = $cache->get($cacheKey);
 * if ($(image === null) {
 *     // Store the image in the cache
 *     $cache->save($cacheKey, file_get_contents('tmp-dir/some-image.jpg'));
 * }
 * header('Content-Type: image/jpeg');
 * echo $image;
 *</code>
 */
class Base64 extends AbstractFrontend
{
  /**
   * Serializes data before storing them
   *
   * @param mixed $data
   *
   * @return string
   */
  public function beforeStore($data)
  {
    return base64_encode($data);
  }

  /**
   * Unserializes data after retrieval
   *
   * @param mixed $data
   *
   * @return mixed
   */
  public function afterRetrieve($data)
  {
    return base64_decode($data);
  }
}
