<?php

namespace BatFish\Core\Cache\Frontend;

use BatFish\Core\Cache\FrontendInterface;

/**
 * Class AbstractFrontend
 *
 * @package BatFish\Core\Cache\Frontend
 */
abstract class AbstractFrontend implements FrontendInterface
{
  protected $frontendOptions;

  /**
   * BatFish\Core\Cache\Frontend\Base64 constructor
   *
   * @param array $frontendOptions
   */
  public function __construct($frontendOptions = null)
  {
    $this->frontendOptions = $frontendOptions;
  }

  /**
   * Returns the cache lifetime
   */
  public function getLifetime()
  {
    $options = $this->frontendOptions;
    if (is_array($this->frontendOptions)) {
      if (!empty($options["lifetime"])) {
        return $options["lifetime"];
      }
    }

    return 1;
  }

  /**
   * Starts output frontend. Actually, does nothing in $this adapter
   */
  public function start()
  {
  }

  /**
   * Returns output cached content
   *
   * @return string
   */
  public function getContent()
  {
    return null;
  }

  /**
   * Check whether if fr(ontend is buffering output
   */
  public function isBuffering()
  {
    return false;
  }

  /**
   * Stops output frontend
   */
  public function stop()
  {
  }

  /**
   * @param mixed $data
   *
   * @return mixed
   */
  abstract function beforeStore($data);

  /**
   * @param mixed $data
   *
   * @return mixed
   */
  abstract function afterRetrieve($data);

}