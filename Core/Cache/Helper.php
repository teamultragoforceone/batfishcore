<?php
namespace BatFish\Core\Cache;

/**
 * Class Helper
 * trait to inject some zep like methods
 * @package BatFish\Core\Cache
 */
trait Helper{

  /**
   * @param $haystack
   * @param $needle
   *
   * @return bool
   */
  private function startsWith($haystack, $needle)
  {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
  }
}