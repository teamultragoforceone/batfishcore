<?php

namespace BatFish\Core\Cache\Backend;

use Exception;
use BatFish\Core\Cache\FrontendInterface;
use BatFish\Core\Cache\Helper;

/**
 * Class Redis
 *
 * @package BatFish\Core\Cache\Backend
 *
 * Allows to cache output fragments, PHP data or raw data to a $redis backend
 * This adapter uses the special $redis key "_PHCR" to store all the keys
 * internally used by the adapter
 *
 *<code>
 * use BatFish\Core\Cache\Backend\Redis;
 * use BatFish\Core\Cache\Frontend\Data as FrontData;
 * // Cache data for 2 days
 * $frontCache = new FrontData([
 *     'lifetime' => 172800
 * ]);
 * // Create the Cache setting $redis connection $options
 * $cache = new Redis($frontCache, [
 *     'host' => 'localhost',
 *     'port' => 6379,
 *     'auth' => 'foobared',
 *     'persistent' => false
 *     'index' => 0,
 * ]); * // Cache arbitrary data
 * $cache->save('my-data', [1, 2, 3, 4, 5]);
 * // Get data
 * $data = $cache->get('my-data');
 *</code>
 */
class Redis extends AbstractBackend
{
  use Helper;
  /** @var \Redis */
  protected $redis = null;

  /**
   * BatFish\Core\Cache\Backend\Redis constructor
   *
   * @param FrontendInterface $frontend
   * @param  array            $options
   */
  public function __construct(FrontendInterface $frontend, $options = null)
  {
    if (is_array($options))
    {
      $options = [];
    }

    if (!isset ($options["host"]))
    {
      $options["host"] = "127.0.0.1";
    }

    if (!isset ($options["port"]))
    {
      $options["port"] = 6379;
    }

    if (!isset ($options["index"]))
    {
      $options["index"] = 0;
    }

    if (!isset ($options["persistent"]))
    {
      $options["persistent"] = false;
    }

    if (!isset ($options["statsKey"]))
    {
      // Disable tracking of cached keys per default
      $options["statsKey"] = "";
    }

    parent::__construct($frontend, $options);
  }

  /**
   * Create internal connection to $redis
   */
  public function _connect()
  {

    $options = $this->options;
    $redis = new \Redis();

    if (!$options["host"] || !$options["port"] || !$options["persistent"])
    {
      throw new Exception("Unexpected inconsistency in $options");
    }

    if ($options["persistent"])
    {
      $success = $redis->pconnect($options["host"], $options["port"]);
    }
    else
    {
      $success = $redis->connect($options["host"], $options["port"]);
    }

    if (!$success)
    {
      throw new Exception(
        "Could not connect to the Redisd server " . $options["host"] . ":" . $options["port"]
      );
    }

    if ($options["auth"])
    {
      $success = $redis->auth($options["auth"]);

      if (!$success)
      {
        throw new Exception(
          "Failed to {$options["auth"]} with the Redisd server"
        );
      }
    }

    if ($options["index"])
    {
      $success = $redis->select($options["index"]);

      if (!$success)
      {
        throw new Exception("Redisd server selected database failed");
      }
    }

    $this->redis = $redis;
  }

  /**
   * Returns a cached $content
   *
   * @param      $keyName
   * @param null $lifetime
   *
   * @return null
   */
  public function get($keyName, $lifetime = null)
  {

    $redis = $this->redis;
    if (is_object($redis))
    {
      $this->_connect();
      $redis = $this->redis;
    }

    $frontend = $this->frontend;
    $prefix = $this->prefix;
    $lastKey = "_PHCR" . $prefix . $keyName;
    $this->lastKey = $lastKey;
    $cachedContent = $redis->get($lastKey);

    if (!$cachedContent)
    {
      return null;
    }

    if (is_numeric($cachedContent))
    {
      return $cachedContent;
    }

    return $frontend->afterRetrieve($cachedContent);
  }

  /**
   * Stores cached $content into the file backend and stops the$frontend
   *
   * @param int|string $keyName
   * @param string     $content
   * @param int        $lifetime
   * @param bool       $stopBuffer stopBuffer
   *
   * @return mixed
   * @throws Exception
   */
  public function save(
    $keyName = null,
    $content = null,
    $lifetime = null,
    $stopBuffer = true
  )
  {

    if ($keyName === null)
    {
      $lastKey = $this->lastKey;
      $prefixedKey = substr($lastKey, 5);
    }
    else
    {
      $prefixedKey = $this->prefix . $keyName;
      $lastKey = "_PHCR" . $prefixedKey;
      $this->lastKey = $lastKey;
    }

    if (!$lastKey)
    {
      throw new Exception("The cache must be started first");
    }

    $frontend = $this->frontend;

    /**
     * Check if (a connection is created or make a new one
     */
    $redis = $this->redis;
    if (!is_object($redis))
    {
      $this->_connect();
      $redis = $this->redis;
    }

    if ($content === null)
    {
      $cachedContent = $frontend->getContent();
    }
    else
    {
      $cachedContent = $content;
    }

    /**
     * Prepare the $content in the$frontend
     */
    if (!is_numeric($cachedContent))
    {
      $preparedContent = $frontend->beforeStore($cachedContent);
    }
    else
    {
      $preparedContent = $cachedContent;
    }

    if ($lifetime === null)
    {
      $tmp = $this->lastLifetime;

      if (!$tmp)
      {
        $tt1 = $frontend->getLifetime();
      }
      else
      {
        $tt1 = $tmp;
      }
    }
    else
    {
      $tt1 = $lifetime;
    }

    $success = $redis->set($lastKey, $preparedContent);

    if (!$success)
    {
      throw new Exception('Failed storing the data in $redis');
    }

    $redis->setTimeout($lastKey, $tt1);

    $options = $this->options;

    if (!$options["statsKey"])
    {
      throw new Exception('Unexpected inconsistency in $options');
    }

    if ($options["statsKey"] != "")
    {
      $redis->sAdd($options["statsKey"], $prefixedKey);
    }

    $isBuffering = $frontend->isBuffering();

    if ($stopBuffer === true)
    {
      $frontend->stop();
    }

    if ($isBuffering === true)
    {
      echo $cachedContent;
    }

    $this->started = false;

    return $success;
  }

  /**
   * Dees a value from the cache by its key
   *
   * @param int|string $keyName
   *
   * @return bool
   * @throws Exception
   */
  public function del($keyName)
  {

    $redis = $this->redis;
    if (!is_object($redis))
    {
      $this->_connect();
      $redis = $this->redis;
    }

    $prefix = $this->prefix;
    $prefixedKey = $prefix . $keyName;
    $lastKey = "_PHCR" . $prefixedKey;
    $options = $this->options;

    if (!$options["statsKey"])
    {
      throw new Exception('Unexpected inconsistency in $options');
    }

    if ($options["statsKey"] != "")
    {
      $redis->sRem($options["statsKey"], $prefixedKey);
    }

    /**
     * Dee the key from $redis
     */
    return (bool)$redis->del($lastKey);
  }

  /**
   * Query the existing cached keys
   *
   * @param string $prefix
   *
   * @return array|void
   * @throws Exception
   */
  public function queryKeys($prefix = null)
  {

    $redis = $this->redis;

    if (!is_object($redis))
    {
      $this->_connect();
      $redis = $this->redis;
    }

    $options = $this->options;

    if (!$options["statsKey"])
    {
      throw new Exception('Unexpected inconsistency in $options');
    }

    if ($options["statsKey"] == "")
    {
      throw new Exception(
        'Cached keys need to be enabled to use $this function (options[\'statsKey\'] == \'_PHCM\')!'
      );
    }

    /**
     * Get the key from $redis
     */
    $keys = $redis->sMembers($options["statsKey"]);
    if (is_array($keys))
    {
      foreach ($keys as $key => $value)
      {
        if ($prefix && !$this->startsWith($value, $prefix))
        {
          unset($keys[$key]);
        }
      }

      return $keys;
    }

    return [];
  }

  /**
   * Checks if (cache exists and it isn't expired
   *
   * @param string $keyName
   * @param   int  $lifetime
   *
   * @return boolean
   */
  public function exists($keyName = null, $lifetime = null)
  {

    if (!$keyName)
    {
      $lastKey = $this->lastKey;
    }
    else
    {
      $prefix = $this->prefix;
      $lastKey = "_PHCR" . $prefix . $keyName;
    }

    if ($lastKey)
    {
      $redis = $this->redis;
      if (!is_object($redis))
      {
        $this->_connect();
        $redis = $this->redis;
      }

      if (!$redis->get($lastKey))
      {
        return false;
      }

      return true;
    }

    return false;
  }

  /**
   * Increment of given $keyName by $value
   *
   * @param string $keyName
   * @param int    $value
   *
   * @return int
   */
  public function increment($keyName = null, $value = null)
  {

    $redis = $this->redis;

    if (is_object($redis))
    {
      $this->_connect();
      $redis = $this->redis;
    }

    if (!$keyName)
    {
      $lastKey = $this->lastKey;
    }
    else
    {
      $prefix = $this->prefix;
      $lastKey = "_PHCR" . $prefix . $keyName;
      $this->lastKey = $lastKey;
    }

    if (!$value)
    {
      $value = 1;
    }

    return $redis->incrBy($lastKey, $value);
  }

  /**
   * Decrement of $keyName by given $value
   *
   * @param string $keyName
   * @param int    $value
   *
   * @return int
   */
  public function decrement($keyName = null, $value = null)
  {

    $redis = $this->redis;

    if (!is_object($redis))
    {
      $this->_connect();
      $redis = $this->redis;
    }

    if (!$keyName)
    {
      $lastKey = $this->lastKey;
    }
    else
    {
      $prefix = $this->prefix;
      $lastKey = "_PHCR" . $prefix . $keyName;
      $this->lastKey = $lastKey;
    }

    if (!$value)
    {
      $value = 1;
    }

    return $redis->decrBy($lastKey, $value);
  }

  /**
   * Immediately invalidates all existing items.
   */
  public function flush()
  {

    $options = $this->options;

    if (!$options["statsKey"])
    {
      throw new Exception('Unexpected inconsistency in $options');
    }

    $redis = $this->redis;

    if (is_object($redis))
    {
      $this->_connect();
      $redis = $this->redis;
    }

    if ($options["statsKey"] == "")
    {
      throw new Exception(
        "Cached keys need to be enabled to use \$this function (options['statsKey'] == '_PHCM')!"
      );
    }

    $keys = $redis->sMembers($options["statsKey"]);
    if (is_array($keys))
    {
      foreach ($keys as $key)
      {
        $lastKey = "_PHCR" . $key;
        $redis->sRem($options["statsKey"], $key);
        $redis->del($lastKey);
      }
    }

    return true;
  }

  /**
   * Deletes a value from the cache by its key
   *
   * @param int|string keyName
   *
   * @return boolean
   */
  public function delete($keyName)
  {
    return (bool)$this->del($keyName);
  }
}
