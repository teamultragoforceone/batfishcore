<?php

namespace BatFish\Core\Cache\Backend;

use DirectoryIterator;
use Exception;
use BatFish\Core\Cache\FrontendInterface;
use BatFish\Core\Cache\Helper;

/**
 * Class File
 *
 * @package BatFish\Core\Cache\Backend
 *
 * Allows to cache output fragments using a file backend
 *
 *<code>
 * use BatFish\Core\Cache\Backend\File;
 * use BatFish\Core\Cache\Frontend\Output as FrontOutput;
 * // Cache the file for 2 days
 * $frontendOptions = [
 *     'lifetime' => 172800
 * ];
 * // Create an output cache
 * $frontCache = FrontOutput($frontOptions);
 * // Set the cache directory
 * $backendOptions = [
 *     'cacheDir' => '../app/cache/'
 * ];
 * // Create the File backend
 * $cache = new File($frontCache, $backendOptions);
 * $content = $cache->start('my-cache');
 * if ($content === null) {
 *     echo '<h1>', time(), '</h1>';
 *     $cache->save();
 * } else {
 *     echo $content;
 * }
 *</code>
 */
class File extends AbstractBackend
{

  use Helper;
  /**
   * Default to false for backwards compatibility
   *
   * @boolean
   */
  private $_useSafeKey = false;

  /**
   * BatFish\Core\Cache\Backend\File constructor
   *
   * @param FrontendInterface $frontend
   * @param array             $options
   *
   * @throws Exception
   */
  public function __construct(FrontendInterface $frontend, array $options)
  {

    if (!isset ($options["cacheDir"]))
    {
      throw new Exception(
        "Cache directory must be specified with the option cacheDir"
      );
    }

    if (isset ($options["safekey"]))
    {
      if (is_bool($options["safekey"]))
      {
        throw new Exception("safekey option should be a boolean.");
      }

      $this->_useSafeKey = $options["safekey"];
    }

    // added to avoid having unsafe filesystem characters in the prefix
    if (isset($options["prefix"]))
    {
      if ($this->_useSafeKey && preg_match(
          "/[^a-zA-Z0-9_.-]+/",
          $options["prefix"]
        )
      )
      {
        throw new Exception(
          "FileCache prefix should only use alphanumeric characters."
        );
      }
    }

    parent::__construct($frontend, $options);
  }

  /**
   * Returns a cached $content
   *
   * @param      $keyName
   * @param null $lifetime
   *
   * @return null|string
   * @throws Exception
   */
  public function get($keyName, $lifetime = null)
  {

    $this->lastKey = $this->prefix . $this->getKey($keyName);

    if (!isset($this->options["cacheDir"]))
    {
      throw new Exception("Unexpected inconsistency in options");
    }

    $cacheFile = $this->options["cacheDir"] . $this->prefix;

    if (file_exists($cacheFile) === true)
    {
      /**hr read it from the set in start()
       */
      if (!$lifetime)
      {
        $lastLifetime = $this->lastLifetime;
        if (!$lastLifetime)
        {
          $ttl = (int)$this->frontend->getLifetime();
        }
        else
        {
          $ttl = (int)$lastLifetime;
        }
      }
      else
      {
        $ttl = (int)$lifetime;
      }

      clearstatcache(true, $cacheFile);
      $modifiedTime = (int)filemtime($cacheFile);

      /**
       * Check if the file has expired
       * The $content is only retrieved if the $content has not expired
       */
      if ($modifiedTime + $ttl > time())
      {

        /**
         * Use file-get-contents to control that the openbase_dir can't be skipped
         */
        $cachedContent = file_get_contents($cacheFile);
        if ($cachedContent === false)
        {
          throw new Exception(
            "Cache file " . $cacheFile . " could not be opened"
          );
        }

        if (is_numeric($cachedContent))
        {
          return $cachedContent;
        }
        else
        {
          /**
           * Use the frontend to process the $content of the cache
           */
          return $this->frontend->afterRetrieve($cachedContent);
        }
      }
    }

    return null;
  }

  /**
   * Stores cached $content into the file backend and stops the frontend
   *
   * @param int|string $keyName
   * @param string     $content
   * @param int        $lifetime
   * @param boolean    $stopBuffer
   *
   * @return int
   * @throws Exception
   */
  public function save(
    $keyName = null,
    $content = null,
    $lifetime = null,
    $stopBuffer = true
  )
  {

    if ($keyName === null)
    {
      $lastKey = $this->lastKey;
    }
    else
    {
      $lastKey = $this->prefix . $this->getKey($keyName);
      $this->lastKey = $lastKey;
    }

    if (!$lastKey)
    {
      throw new Exception("Cache must be started first");
    }

    if (!isset($this->options["cacheDir"]))
    {
      throw new Exception("Unexpected inconsistency in options");
    }

    $cacheFile = $this->options["cacheDir"] . $lastKey;

    if ($content === null)
    {
      $cachedContent = $this->frontend->getContent();
    }
    else
    {
      $cachedContent = $content;
    }

    if (!is_numeric($cachedContent))
    {
      $preparedContent = $this->frontend->beforeStore($cachedContent);
    }
    else
    {
      $preparedContent = $cachedContent;
    }

    /**
     * We use file_put_contents to respect open-base-dir directive
     */
    $status = file_put_contents($cacheFile, $preparedContent);

    if ($status === false)
    {
      throw new Exception("Cache file " . $cacheFile . " could not be written");
    }

    $isBuffering = $this->frontend->isBuffering();

    if ($stopBuffer === true)
    {
      $this->frontend->stop();
    }

    if ($isBuffering === true)
    {
      echo $cachedContent;
    }

    $this->started = false;

    return $status;
  }

  /**
   * Deletes a value from the cache by its key
   *
   * @param int|string $keyName
   *
   * @return bool
   * @throws Exception
   */
  public function delete($keyName)
  {

    if (!isset($this->options["cacheDir"]))
    {
      throw new Exception("Unexpected inconsistency in options");
    }

    $cacheFile = $this->options["cacheDir"] . $this->prefix . $this->getKey(
        $keyName
      );
    if (file_exists($cacheFile))
    {
      return unlink($cacheFile);
    }

    return false;
  }

  /**
   * Query the existing cached keys
   *
   * @param string|int prefix
   *
   * @return array
   * @throws Exception
   */
  public function queryKeys($prefix = null)
  {
    $keys = [];

    if (!isset($this->options["cacheDir"]))
    {
      throw new Exception("Unexpected inconsistency in options");
    }
    /**
     * We use a directory iterator to traverse the cache dir directory
     */
    $cacheDir = new \DirectoryIterator($this->options["cacheDir"]);
    foreach ($cacheDir as $key => $item)
    {

      if ($item->isDir() === false)
      {
        $key = $item->getFilename();
        if ($prefix !== null)
        {
          if ($this->startsWith($key, $prefix))
          {
            $keys[] = $key;
          }
        }
        else
        {
          $keys[] = $key;
        }
      }
    }

    return $keys;
  }

  /**
   * Checks if cache exists and it isn't expired
   *
   * @param string|int $keyName
   * @param   int      $lifetime
   *
   * @return boolean
   */
  public function exists($keyName = null, $lifetime = null)
  {

    if (!$keyName)
    {
      $lastKey = $this->lastKey;
    }
    else
    {
      $lastKey = $this->prefix . $this->getKey($keyName);
    }

    if ($lastKey)
    {

      $cacheFile = $this->options["cacheDir"] . $lastKey;

      if (file_exists($cacheFile))
      {

        /**
         * Check if the file has expired
         */
        if (!$lifetime)
        {
          $ttl = (int)$this->frontend->getLifetime();
        }
        else
        {
          $ttl = (int)$lifetime;
        }

        clearstatcache(true, $cacheFile);
        $modifiedTime = (int)filemtime($cacheFile);

        if ($modifiedTime + $ttl > time())
        {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Increment of a given key, by number $value
   *
   * @param  string|int $keyName
   * @param  int        $value
   *
   * @return mixed
   * @throws Exception
   */
  public function increment($keyName = null, $value = 1)
  {

    $prefixedKey = $this->prefix . $this->getKey($keyName);
    $this->lastKey = $prefixedKey;
    $cacheFile = $this->options["cacheDir"] . $prefixedKey;

    if (file_exists($cacheFile))
    {

      /**
       * Take the lifetime from the frontend or read it from the set in start()
       */
      $lifetime = $this->lastLifetime;
      if (!$lifetime)
      {
        $ttl = $this->frontend->getLifetime();
      }
      else
      {
        $ttl = $lifetime;
      }

      clearstatcache(true, $cacheFile);
      $modifiedTime = (int)filemtime($cacheFile);

      /**
       * Check if the file has expired
       * The $content is only retrieved if the $content has not expired
       */
      if ($modifiedTime + $ttl > time())
      {

        /**
         * Use file-get-contents to control that the openbase_dir can't be skipped
         */
        $cachedContent = file_get_contents($cacheFile);

        if ($cachedContent === false)
        {
          throw new Exception(
            "Cache file " . $cacheFile . " could not be opened"
          );
        }

        if (is_numeric($cachedContent))
        {

          $result = $cachedContent + $value;
          if (file_put_contents($cacheFile, $result) === false)
          {
            throw new Exception("Cache directory could not be written");
          }

          return $result;
        }
      }
    }
    return false;
  }

  /**
   * Decrement of a given key, by number $value
   *
   * @param  string|int $keyName
   * @param int         $value value
   *
   * @return mixed
   * @throws Exception
   */
  public function decrement($keyName = null, $value = 1)
  {

    $prefixedKey = $this->prefix . $this->getKey($keyName);
    $this->lastKey = $prefixedKey;
    $cacheFile = $this->options["cacheDir"] . $prefixedKey;

    if (file_exists($cacheFile))
    {

      /**
       * Take the lifetime from the frontend or read it from the set in start()
       */
      $lifetime = $this->lastLifetime;
      if (!$lifetime)
      {
        $ttl = $this->frontend->getLifetime();
      }
      else
      {
        $ttl = $lifetime;
      }

      clearstatcache(true, $cacheFile);
      $modifiedTime = (int)filemtime($cacheFile);

      /**
       * Check if the file has expired
       * The $content is only retrieved if the $content has not expired
       */
      if ($modifiedTime + $ttl > time())
      {

        /**
         * Use file-get-contents to control that the openbase_dir can't be skipped
         */
        $cachedContent = file_get_contents($cacheFile);

        if ($cachedContent === false)
        {
          throw new Exception(
            "Cache file " . $cacheFile . " could not be opened"
          );
        }

        if (is_numeric($cachedContent))
        {

          $result = $cachedContent - $value;
          if (file_put_contents($cacheFile, $result) === false)
          {
            throw new Exception("Cache directory can't be written");
          }

          return $result;
        }
      }
    }
    return false;
  }

  /**
   * Immediately invalidates all existing items.
   */
  public function flush()
  {

    $prefix = $this->prefix;

    if (!isset($this->options["cacheDir"]))
    {
      throw new Exception("Unexpected inconsistency in options");
    }

    foreach ((new DirectoryIterator($this->options["cacheDir"])) as $item)
    {
      if ($item->isFile() == true)
      {
        $key = $item->getFilename();
					$cacheFile = $item->getPathname();

				if (empty ($prefix) || $this->startsWith($key, $prefix))
        {
          if (!unlink($cacheFile))
          {
            return false;
          }
        }
			}
    }

    return true;
  }

  /**
   * Return a file-system safe identifier for a given key
   *
   * @param $key
   *
   * @return string
   */
  public function getKey($key)
  {
    if ($this->_useSafeKey === true)
    {
      return md5($key);
    }

    return $key;
  }
 }
