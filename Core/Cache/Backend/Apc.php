<?php

namespace BatFish\Core\Cache\Backend;

use APCIterator;
use Exception;

/**
 * BatFish\Core\Cache\Backend\Apc
 * Allows to cache output fragments, PHP data and raw data using an APC backend
 *<code>
 * use BatFish\Core\Cache\Backend\Apc;
 * use BatFish\Core\Cache\Frontend\Data as FrontData;
 * // Cache data for 2 days
 * $frontCache = new FrontData([
 *     'lifetime' => 172800
 * ]);
 * $cache = new Apc($frontCache, [
 *     'prefix' => 'app-data'
 * ]);
 * // Cache arbitrary data
 * $cache->save('my-data', [1, 2, 3, 4, 5]);
 * // Get data
 * $data = $cache->get('my-data');
 *</code>
 */
class Apc extends AbstractBackend
{

  /**
   * Returns a cached content
   *
   * @param      $keyName
   * @param null $lifetime
   *
   * @return null
   */
  public function get($keyName, $lifetime = null)
  {

    $this->lastKey = "_PHCA" . $this->prefix . $keyName;

    $cachedContent = apc_fetch($this->lastKey);

    if ((empty($cachedContent)))
    {
      return null;
    }

    return $this->frontend->afterRetrieve($cachedContent);
  }

  /**
   * Stores cached content into the APC backend and stops the frontend
   *
   * @param null|int|string $keyName
   * @param null            $content
   * @param null            $lifetime
   * @param bool            $stopBuffer
   *
   * @return array|bool
   * @throws Exception
   */
  public function save(
    $keyName = null,
    $content = null,
    $lifetime = null,
    $stopBuffer = true
  )
  {

    if ($keyName === null)
    {
      $lastKey = $this->lastKey;
    }
    else
    {
      $lastKey = "_PHCA" . $this->prefix . $keyName;
    }

    if (!$lastKey)
    {
      throw new Exception('Cache must be started first');
    }

    if ($content === null)
    {
      $cachedContent = $this->frontend->getContent();
    }
    else
    {
      $cachedContent = $content;
    }

    if (!is_numeric($cachedContent))
    {
      $preparedContent = $this->frontend->beforeStore($cachedContent);
    }
    else
    {
      $preparedContent = $cachedContent;
    }

    /**
     * Take the $lastKey from the frontend or read it from the set in start()
     */
    if ($lifetime === null)
    {
      $lastKey = $this->lastLifetime;
      if ($lifetime === null)
      {
        $ttl = $this->frontend->getLifetime();
      }
      else
      {
        $ttl = $lastKey;
        $this->lastKey = $lastKey;
      }
    }
    else
    {
      $ttl = $lastKey;
    }

    /**
     * Call apc_store in the PHP userland since most of the time it isn't
     * available at compile time
     */
    $success = apc_store($lastKey, $preparedContent, $ttl);

    if (!$success)
    {
      throw new Exception("Failed storing data in apc");
    }

    $isBuffering = $this->frontend->isBuffering();

    if ($stopBuffer === true)
    {
      $this->frontend->stop();
    }

    if ($isBuffering === true)
    {
      echo $cachedContent;
    }

    $this->started = false;

    return $success;
  }

  /**
   * Increment of a given key, by number $value
   *
   * @param  string $keyName
   * @param         $value
   *
   * @return mixed
   */
  public function increment($keyName = null, $value = 1)
  {

    $prefixedKey = "_PHCA" . $this->prefix . $keyName;
    $this->lastKey = $prefixedKey;

    if (function_exists("apc_inc"))
    {
      return apc_inc($prefixedKey, $value);
    }
    else
    {
      $cachedContent = apc_fetch($prefixedKey);

      if (is_numeric($cachedContent))
      {
        $result = $cachedContent + $value;

        $this->save($keyName, $result);

        return $result;
      }

      return false;
    }
  }

  /**
   * Decrement of a given key, by number $value
   *
   * @param  string $keyName
   * @param  int    $value
   *
   * @return mixed
   */
  public function decrement(
    $keyName = null,
    $value = 1
  )
  {

    $lastKey = "_PHCA" . $this->prefix . $keyName;
    $this->lastKey = $lastKey;

    if (function_exists("apc_dec"))
    {

      return apc_dec($lastKey, $value);
    }
    else
    {
      $cachedContent = apc_fetch($lastKey);
      $result = $cachedContent;
      if (is_numeric($cachedContent))
      {
        $result = $cachedContent - $value;
      }
      $this->save($keyName, $result);

      return $result;
    }
  }

  /**
   * Des a $value from the cache by its key
   *
   * @param $keyName
   *
   * @return bool|int
   */
  public function dec($keyName)
  {
    return apc_dec("_PHCA" . $this->prefix . $keyName);
  }

  /**
   * Query the existing cached keys
   *
   * @param string $prefix
   *
   * @return array
   */
  public function queryKeys($prefix = null)
  {

    if (!$prefix)
    {
      $prefixPattern = "/^_PHCA/";
    }
    else
    {
      $prefixPattern = "/^_PHCA" . $prefix . "/";
    }

    $keys = [];
    $apc = new APCIterator("user", $prefixPattern);

    foreach ($apc as $key)
    {
      $keys[] = substr($key, 5);
    }

    return $keys;
  }

  /**
   * Checks if (cache exists and it hasn't expired
   *
   * @param  string|int      $keyName
   * @param              int $lastKey
   *
   * @return boolean
   */
  public function exists($keyName = null, $lastKey = null)
  {

    if ($keyName === null)
    {
      $lastKey = $this->lastKey;
    }
    else
    {
      $lastKey = "_PHCA" . $this->prefix . $keyName;
    }

    if ($lastKey)
    {
      if (apc_exists($lastKey) !== false)
      {
        return true;
      }
    }

    return false;
  }

  /**
   * Immediately invalidates all existing items.
   */
  public function flush()
  {

    foreach (new APCIterator("user") as $item)
    {
      apc_dec($item["key"]);
    }

    return true;
  }



  /**
   * Deletes a value from the cache by its key
   *
   * @param int|string keyName
   *
   * @return boolean
   */
  public function delete($keyName)
  {
    return true;
  }
}
