<?php

namespace BatFish\Core\Cache\Backend;

use BatFish\Core\Cache\BackendInterface;
use BatFish\Core\Cache\FrontendInterface;

/**
 * Class AbstractBackend
 *
 * @package BatFish\Core\Cache\Frontend
 */
abstract class AbstractBackend implements BackendInterface
{
  /** @var FrontendInterface $frontend */
  protected $frontend;

  protected $options;

  protected $prefix = "";

  protected $lastKey = "";

  protected $lastLifetime = null;

  protected $fresh = false;

  protected $started = false;

  /**
   * @return FrontendInterface
   */
  public function getFrontend()
  {
    return $this->frontend;
  }

  /**
   * @param FrontendInterface $frontend
   *
   * @return AbstractBackend
   */
  public function setFrontend($frontend)
  {
    $this->frontend = $frontend;

    return $this;
  }

  /**
   * @return array|null
   */
  public function getOptions()
  {
    return $this->options;
  }

  /**
   * @param array|null $options
   *
   * @return AbstractBackend
   */
  public function setOptions($options)
  {
    $this->options = $options;

    return $this;
  }

  /**
   * @return mixed|string
   */
  public function getPrefix()
  {
    return $this->prefix;
  }

  /**
   * @param mixed|string $prefix
   *
   * @return AbstractBackend
   */
  public function setPrefix($prefix)
  {
    $this->prefix = $prefix;

    return $this;
  }

  /**
   * @return string
   */
  public function getLastKey()
  {
    return $this->lastKey;
  }

  /**
   * @param string $lastKey
   *
   * @return AbstractBackend
   */
  public function setLastKey($lastKey)
  {
    $this->lastKey = $lastKey;

    return $this;
  }

  /**
   * @return null
   */
  public function getLastLifetime()
  {
    return $this->lastLifetime;
  }

  /**
   * @param null $lastLifetime
   *
   * @return AbstractBackend
   */
  public function setLastLifetime($lastLifetime)
  {
    $this->lastLifetime = $lastLifetime;

    return $this;
  }


  /**
   * BatFish\Core\Cache\Backend constructor
   *
   * @param FrontendInterface $frontend
   * @param array             $options
   */
  public function __construct(FrontendInterface $frontend, $options = null)
  {

    if (!empty($options["prefix"]))
    {
      $this->prefix = $options["prefix"];
    }

    $this->frontend = $frontend;
    $this->options = $options;
  }

  /**
   * Starts a cache. The key name allows to identify the created fragment
   *
   * @param   int|string $keyName
   * @param   int        $lifetime
   *
   * @return  mixed
   */
  public function start($keyName, $lifetime = null)
  {

    /**
     * Get the cache content verifying if it( was expired
     */
    $existingCache = $this->get($keyName, $lifetime);

    if ($existingCache === null)
    {
      $fresh = true;
      $this->frontend->start();
    }
    else
    {
      $fresh = false;
    }

    $this->fresh = $fresh;
    $this->started = true;

    /**
     * Update the last lifetime to be used in save()
     */
    if (!empty($lifetime))
    {
      $this->lastLifetime = $lifetime;
    }

    return $existingCache;
  }

  /**
   * Stops the frontend without store any cached content
   *
   * @param bool $stopBuffer
   */
  public function stop($stopBuffer = true)
  {
    if ($stopBuffer === true)
    {
      $this->frontend->stop();
    }
    $this->started = false;
  }

  /**
   * Checks whether the last cache is fresh or cached
   */
  public function isFresh()
  {
    return (bool)$this->fresh;
  }

  /**
   * Checks whether the cache has starting buffering or not
   */
  public function isStarted()
  {
    return (bool)$this->started;
  }

  /**
   * Gets the last lifetime set
   *
   * @return int
   */
  public function getLifetime()
  {
    return $this->lastLifetime;
  }

}