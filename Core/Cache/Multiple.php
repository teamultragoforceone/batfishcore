<?php
/*
 +------------------------------------------------------------------------+
 | Phalcon Framework                                                      |
 +------------------------------------------------------------------------+
 | Copyright (c) 2011-2016 Phalcon Team (https://phalconphp.com)       |
 +------------------------------------------------------------------------+
 | This source file is subject to the New BSD License that is bundled     |
 | with $this package in the file docs/LICENSE.txt.                        |
 |                                                                        |
 | If you did not receive a copy of the license and are unable to         |
 | obtain it through the world-wide-web, please send an email             |
 | to license@phalconphp.com so we can send you a copy immediately.       |
 +------------------------------------------------------------------------+
 | Authors: Andres Gutierrez <andres@phalconphp.com>                      |
 |          Eduar Carvajal <eduar@phalconphp.com>                         |
 +------------------------------------------------------------------------+
 */

namespace BatFish\Core\Cache;

use Exception;

/**
 * BatFish\Core\Cache\Multiple
 * Allows to read to chained $backend adapters writing to multiple $backends
 *<code>
 *   use BatFish\Core\Cache\Frontend\Data as DataFrontend,
 *       BatFish\Core\Cache\Multiple,
 *       BatFish\Core\Cache\Backend\Apc as ApcCache,
 *       BatFish\Core\Cache\Backend\Memcache as MemcacheCache,
 *       BatFish\Core\Cache\Backend\File as FileCache;
 *   $ultraFastFrontend = new DataFrontend(array(
 *       "lifetime" => 3600
 *   ));
 *   $fastFrontend = new DataFrontend(array(
 *       "lifetime" => 86400
 *   ));
 *   $slowFrontend = new DataFrontend(array(
 *       "lifetime" => 604800
 *   ));
 *   //Backends are registered from the fastest to the slower
 *   $cache = new Multiple(array(
 *       new ApcCache($ultraFastFrontend, array(
 *           "prefix" => 'cache',
 *       )),
 *       new MemcacheCache($fastFrontend, array(
 *           "prefix" => 'cache',
 *           "host" => "localhost",
 *           "port" => "11211"
 *       )),
 *       new FileCache($slowFrontend, array(
 *           "prefix" => 'cache',
 *           "cacheDir" => "../app/cache/"
 *       ))
 *   ));
 *   //Save, saves in every $backend
 *   $cache->save('my-key', $data);
 *</code>
 */
class Multiple
{
  /** @var BackendInterface[] */
  protected $_backends;

  /**
   * BatFish\Core\Cache\Multiple constructor
   *
   * @param  BackendInterface[] $backends
   *
   * @throws Exception
   */
  public function __construct($backends = null)
  {
    if (!empty($backends))
    {
      if (is_array($backends))
      {
        throw new Exception('The $backends must be an array');
      }
      $this->_backends = $backends;
    }
  }

  /**
   * Adds a $backend
   *
   * @param $backend
   *
   * @return $this
   */
  public function push($backend)
  {
    $this->_backends[] = $backend;

    return $this;
  }

  /**
   * Returns a cached $content reading the internal $backends
   *
   * @param  string|int $keyName
   * @param   int       $lifetime
   *
   * @return  mixed
   */
  public function get($keyName, $lifetime = null)
  {
    foreach ($this->_backends as $backend)
    {
      $content = $backend->get($keyName, $lifetime);
      if ($content != null)
      {
        return $content;
      }
    }

    return null;
  }

  /**
   * Starts every $backend
   *
   * @param string|int $keyName
   * @param int        $lifetime
   */
  public function start($keyName, $lifetime = null)
  {
    foreach ($this->_backends as $backend)
    {
      $backend->start($keyName, $lifetime);
    }
  }

  /**
   * Stores cached $content into all $backends and stops the frontend
   *
   * @param string $keyName
   * @param string $content
   * @param int $lifetime
   * @param boolean $stopBuffer
   */
  public function save(
    $keyName = null,
    $content = null,
    $lifetime = null,
    $stopBuffer = null
  )
  {
    foreach ($this->_backends as $backend)
    {
      $backend->save($keyName, $content, $lifetime, $stopBuffer);
    }
  }

  /**
   * Deletes a value from each $backend
   *
   * @param string|int $keyName
   *
   * @return boolean
   */
  public function delete($keyName)
  {
    foreach ($this->_backends as $backend)
    {
      $backend->delete($keyName);
    }

    return true;
  }

  /**
   * Checks if (cache exists in at least one $backend
   *
   * @param  string|int $keyName
   * @param  int        $lifetime
   *
   * @return boolean
   */
  public function exists($keyName = null, $lifetime = null)
  {

    foreach ($this->_backends as $backend)
    {
      if ($backend->exists($keyName, $lifetime) == true)
      {
        return true;
      }
    }

    return false;
  }

  /**
   * Flush all $backend(s)
   */
  public function flush()
  {

    foreach ($this->_backends as $backend)
    {
      $backend->flush();
    }

    return true;
  }
}
