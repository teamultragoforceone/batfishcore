<?php
namespace BatFish\Core\Domain;

use \BatFish\Core\Module\Modules;
use BatFish\Core\Config\AbstractConfig;
use BatFish\Core\Flash\Direct;
use BatFish\Core\Flash\Flash;
use BatFish\Core\Flash\Session as FlashSession;
use BatFish\Core\Http\Request;
use BatFish\Core\Http\Response;
use BatFish\Core\Session\Session;
use BatFish\Core\User\User;

/**
 * Class Domain
 *
 * @package BatFish\Core\Domain
 */
class Domain implements DomainInterface
{
  use DomainAwareTrait;
  /** @var  Session */
  protected $session;
  /** @var  Request */
  protected $request;
  /** @var  Response */
  protected $response;
  /** @var  Modules */
  protected $modules;
  /** @var  Flash */
  protected $flash;
  /** @var  User */
  protected $user;
  /** @var  AbstractConfig */
  protected $config;

  /**
   * @return AbstractConfig
   */
  public function getConfig(): AbstractConfig
  {
    return $this->config;
  }

  /**
   * @param AbstractConfig $config
   *
   * @return Domain
   */
  public function setConfig(AbstractConfig $config): Domain
  {
    $this->config = $config;

    return $this;
  }

  /**
   * @return Modules
   */
  public function getModules(): Modules
  {
    if (!$this->modules instanceof Modules)
    {
      $this->setModules(
        (new Modules())
          ->setDomain(
            $this
          )
      );
    }

    return $this->modules;
  }

  /**
   * @param Modules $modules
   *
   * @return Domain
   */
  protected function setModules(Modules $modules): Domain
  {
    $this->modules = $modules;

    return $this;
  }

  /**
   * @return Session
   */
  public function getSession(): Session
  {
    if (!$this->session)
    {
      $this->setSession(
        (new Session())
          ->setDomain($this)
      );
      $this->session->start();
    }

    return $this->session;
  }

  /**
   * @param Session $session
   */
  protected function setSession(Session $session)
  {
    $this->session = $session;
  }

    /**
     * @param bool $useSession
     * @return Flash
     */
  public function getFlash($useSession = false): Flash
  {
    if (!$this->flash)
    {
        if($useSession){
            $this->setFlash(
                (new FlashSession())->setDomain($this->getDomain())
            );
        }else{
            $this->setFlash(
                new Direct()
            );
        }

    }

    return $this->flash;
  }

  /**
   * @param Flash $session
   */
  protected function setFlash(Flash $session)
  {
    $this->flash = $session;
  }

  /**
   * @return Request
   */
  public function getRequest(): Request
  {
    if (!$this->request)
    {
      $this->setRequest(
        (new Request())
          ->setDomain($this)
      );
    }

    return $this->request;
  }

  /**
   * @param Request $request
   *
   * @return Domain
   */
  protected function setRequest($request)
  {
    $this->request = $request;

    return $this;
  }

  /**
   * @return Response
   */
  public function getResponse(): Response
  {
    if (!$this->response)
    {
      $this->setResponse(
          (new Response())
      );
    }

    return $this->response;
  }

  /**
   * @param Response $response
   *
   * @return Domain
   */
  protected function setResponse($response)
  {
    $this->response = $response;

    return $this;
  }

  /**
   * @return User
   */
  public function getUser(): User
  {
    if (!$this->user)
    {
      $this->setUser(
          (new User())
              ->setDomain($this)
      );
    }
    return $this->user;
  }

  /**
   * @param User $user
   */
  protected function setUser(User $user)
  {

    $this->user = $user;
  }

  public function systemLog($message , $level = 'info'){
    $this->getModules()->getSystemModule()->log($message , $level);
  }

}