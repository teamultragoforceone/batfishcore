<?php
namespace BatFish\Core\Domain;
use BatFish\Core\Exceptions\AbstractBatfishException;


/**
 * Class \BatFish\Core\Domain\DomainException
 *
 * @package BatFish\Core\Domain
 */
class DomainException extends AbstractBatfishException
{


}