<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 19/11/16
 * Time: 09:44
 */

namespace BatFish\Core\Domain;


abstract class Component implements ComponentInterface
{
  use DomainAwareTrait;

}