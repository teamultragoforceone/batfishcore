<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 18/11/16
 * Time: 22:38
 */

namespace BatFish\Core\Domain;


interface DomainAwareInterface
{
  public function getDomain();
  /**
   * @param DomainInterface $domain
   * @return $this
   */
  public function setDomain(DomainInterface $domain);
}