<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 18/11/16
 * Time: 22:37
 */

namespace BatFish\Core\Domain;

trait DomainAwareTrait
{
  /** @var  Domain */
  protected $domain;

  /**
   * @return Domain
   * @throws DomainException
   */
  public function getDomain()
  {
    if(!$this->domain instanceof DomainInterface){
        throw new DomainException('domain not set ');
    }
    return $this->domain;
  }

  /**
   * @param DomainInterface $domain
   * @return $this
   */
  public function setDomain(DomainInterface $domain)
  {
    $this->domain = $domain;
    return $this;
  }


}