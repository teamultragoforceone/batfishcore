<?php


namespace BatFish\Core\Route;


interface RouteInterface
{
  public static function addRoute($request, $route, $controllers);

  public static function getRoutes();

  public static function get($route, $controllers);

  public static function post($route, $controllers);

  public static function put($route, $controllers);

  public static function delete($route, $controllers);

  public static function notFound($controllers);
}