<?php

namespace BatFish\Core\Route;

use BatFish\Core\Config\Config;
use BatFish\Core\Exceptions\BatfishLayoutException;

class Route implements RouteInterface
{

  /**
   * @var array
   */
  protected static $routes;
  public static $domain;
    /**
     * @param $group
     * @param $routes
     */
    public static function group(string $group , $routes =[] )
    {
        foreach ($routes as $route){
            $middleware = null; 
            if(isset($route['middleware'])){
                $middleware = $route['middleware'] ;
            }
            Route::addRouteToGroup($group,'GET',  $route[0],$routes[1],$middleware);
            Route::addRouteToGroup($group,'POST',   $route[0],$routes[1],$middleware);
            Route::addRouteToGroup($group,'PUT',  $route[0],$routes[1],$middleware);
            Route::addRouteToGroup($group,'DELETE' ,  $route[0],$routes[1],$middleware);
        }

    }

  /**
   * @param $route
   * @param $controllers
   */
  public static function request($route, $controllers )
  {
      $middleware = null;
      if(isset($route['middleware'])){
          $middleware = $route['middleware'] ;
      }
    Route::addRoute('GET', $route, $controllers,$middleware);
    Route::addRoute('POST', $route, $controllers,$middleware);
    Route::addRoute('PUT', $route, $controllers,$middleware);
    Route::addRoute('DELETE', $route, $controllers,$middleware);
  }

  /**
   * @param $route
   * @param $controllers
   */
  public static function get($route, $controllers)
  {
    Route::addRoute('GET', $route, $controllers);
  }

  /**
   * @param $route
   * @param $controllers
   */
  public static function post($route, $controllers)
  {
    Route::addRoute('POST', $route, $controllers);
  }

  /**
   * @param $route
   * @param $controllers
   */
  public static function put($route, $controllers)
  {
    Route::addRoute('PUT', $route, $controllers);
  }

  /**
   * @param $route
   * @param $controllers
   */
  public static function delete($route, $controllers)
  {
    Route::addRoute('DELETE', $route, $controllers);
  }

  public static function notFound($controllers)
  {
    Route::addRoute('404', '', $controllers);
  }

    /**
     * @param $request
     * @param $route
     * @param $controllers
     * @param null $middleware
     */
  public static function addRoute($request, $route, $controllers, $middleware= null)
  {
    Route::$routes[$request][$route] = $controllers;
  }

    /**
     * @param $gorup
     * @param $request
     * @param $route
     * @param $controllers
     * @internal param null $middleware
     */
    public static function addRouteToGroup($gorup, $request, $route, $controllers)
    {
        Route::$routes[$gorup][$request][$route] = $controllers;
    }
  /**
   * @return array
   */
  public static function getRoutes():array
  {
    return Route::$routes;
  }

  /**
   * @param $request
   * @param $route
   * @return array
   */
  public static function getRoute($request, $route):array
  {

    //$request = str_replace('/index.php','',$request);

    $routes = Route::getRoutes();

    if ($key = static::pregArrayKeyExists($route, $routes[$request]))
    {
        Route::prosesMiddleware($request,$key);



      return $routes[$request][$key];
    }

    return [];
  }

  /**
   * @param $request
   * @param $routes
   *
   * @return bool
   * @internal param $pattern
   * @internal param $array
   */
  protected  static function pregArrayKeyExists($request, $routes) {

    // extract the keys.
    $keys = array_keys($routes);
    // convert the preg_grep() returned array to int..and return.
    // the ret value of preg_grep() will be an array of values
    // that match the pattern.
    $maces = [];
    foreach ($keys  as $pattern){
      preg_match("@$pattern@",$request, $out);
      if($out){
      $maces[] = $pattern;
      }
    }
    if($maces){
      return array_pop($maces);
    }
    return false;


  }
  protected static function prosesMiddleware($request, $route){
if(isset(Route::$routes[$request][$route]['middleware'])){

   $middlewares = Route::$routes[$request][$route]['middleware'];
    if(!is_array($middlewares)){
        throw new BatfishLayoutException('rousght mindle were must be an arry');
    }
    foreach ($middlewares as $middleware){
        /** @var \BatFish\Core\Middleware\AbstractMiddleware $mid */
        $mid =  (new $middleware);
        if($mid instanceof \BatFish\Core\Middleware\AbstractMiddleware ){
            throw new BatfishLayoutException('rousght mindle were must be an arry');
        }
        $mid->run();
    }
}

  }

}