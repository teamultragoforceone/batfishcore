<?php

namespace BatFish\Core\Route;

use BatFish\Core\Domain\DomainAwareTrait;
use BatFish\Core\Layout\CoreLayout;
use InvalidArgumentException;
use ReflectionClass;

class Router implements RouterInterface
  {
  use  DomainAwareTrait;

  protected $controller;
  protected $action;
  protected $params;
  protected $basePath;
  protected $requestMethod;
  protected $path;

  /** @var CoreLayout */
  protected $layout;

  /**
   * @return mixed
   */
  public function getBasePath()
  {
    return $this->basePath;
  }

  /**
   * @param mixed $basePath
   *
   * @return Router
   */
  public function setBasePath($basePath)
  {
    $this->basePath = $basePath;

    return $this;
  }

  public function run()
  {
    $this->render();
    //include './../App/View/' . $this->getLayout()->getBaseLayoutPath();
  }

  protected function render()
  {
    $controllers = $this->uriToControllers();

    $this
      ->getLayout()
      ->render(
        $this->getRequestMethod(),
        $controllers
      );
  }

  protected function uriToControllers(): array
  {
    $controllers = Route::getRoute(
      $this->getRequestMethod(),
      $this->getPath()
    );

    if(empty($controllers)) {
      $controllers = Route::getRoute('404', '');
    }

    return $controllers;
  }

  /**
   * @return mixed
   */
  public function getRequestMethod()
  {

    if(empty($this->requestMethod)) {
      $this->setRequestMethod(
        $_SERVER['REQUEST_METHOD']
      );
    }

    return $this->requestMethod;
  }

  /**
   * @param mixed $requestMethod
   *
   * @return Router
   */
  public function setRequestMethod($requestMethod)
  {
    $this->requestMethod = $requestMethod;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getPath()
  {
    if(empty($this->path)) {

      $this->setPath($_SERVER['REQUEST_URI']);
    }

    return $this->path;
  }

  /**
   * @param mixed $path
   *
   * @return Router
   */
  public function setPath($path)
  {
    $this->path = $path;

    return $this;
  }

  /**
   * @return CoreLayout
   */
  public function getLayout(): CoreLayout
  {

    return $this->layout;
  }

  /**
   * @param $layout
   */
  public function setLayout($layout)
  {
    $this->layout = $layout;
  }

  /**
   * @param $controller
   *
   * @return $this
   * @throws \InvalidArgumentException
   */
  public function setController($controller)
  {
    $controller = ucfirst(strtolower($controller)) . 'Controller';

    if(!class_exists($controller)) {
      throw new InvalidArgumentException(
        "The action controller '$controller' has not been defined.");
    }
    $this->controller = $controller;

    return $this;
  }

  /**
   * @param $action
   *
   * @return $this
   * @throws \InvalidArgumentException
   */
  public function setAction($action)
  {
    $reflector = new ReflectionClass($this->controller);

    if(!$reflector->hasMethod($action)) {
      throw new InvalidArgumentException(
        "The controller action '$action' has been not defined.");
    }

    $this->action = $action;

    return $this;
  }

  /**
   * @param array $params
   *
   * @return $this
   */
  public function setParams(array $params)
  {
    $this->params = $params;

    return $this;
  }
  }
