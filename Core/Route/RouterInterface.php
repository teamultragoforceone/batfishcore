<?php

namespace BatFish\Core\Route;

use BatFish\Core\Domain\DomainAwareInterface;

interface RouterInterface extends DomainAwareInterface
{
  public function setController($controller);
  public function setAction($action);
  public function setParams(array $params);
  public function run();
}