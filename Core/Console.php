<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 16/09/17
 * Time: 16:29
 */

namespace BatFish\Core;

use BatFish\Core\Console\Application;

class Console extends Application {
    /* init your application options here */
    /**
     * @param \GetOptionKit\OptionCollection $opts
     */
    public function options($opts)
    {
        parent::options($opts);
        $opts->add('v|verbose', 'verbose message');
     //$opts->add('path:', 'required option with a value.');
     $opts->add('path?', 'optional option with a value');
     //$opts->add('path+', 'multiple value option.');
    }

    /* register your command here */
    public function init()
    {
        parent::init();
    }
}