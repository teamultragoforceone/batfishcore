<?php

namespace BatFish\Core;


use BatFish\Core\Route\Router;

interface AppInterface
{
  /**
   * @return mixed
   */
  public function getRoutes();

  /**
   * @param mixed $routes
   * @return AppInterface
   */
  public function setRoutes($routes);

  /**
   *
   * @return mixed
   */
  function run();

  /**
   * @return mixed
   */
  public function getPublicPath();

  /**
   * @param mixed $publicPath
   * @return $this
   */
  public function setPublicPath($publicPath);

  /**
   * @return mixed
   */
  public function getRootPath();

  /**
   * @param mixed $rootPath
   * @return $this
   */
  public function setRootPath($rootPath);

  /**
   * @return mixed
   */
  public function getAppPath();

  /**
   * @param mixed $appPath
   * @return $this
   */
  public function setAppPath($appPath);

  /**
   * @return mixed
   */
  public function getRouter();

  /**
   * @param Router $router
   * @return mixed
   */
  public function setRouter(Router $router);
}
