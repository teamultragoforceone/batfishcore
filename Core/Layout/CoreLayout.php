<?php


namespace BatFish\Core\Layout;


use BatFish\Core\Controller\ControllerPrototype;
use BatFish\Core\Domain\DomainAwareTrait;

abstract class CoreLayout implements CoreLayoutInterface
{
  use DomainAwareTrait;

  private $path;
  abstract public function init();
  /**
   * @param $request
   * @param $controllers
   */
  public function render($request, $controllers)
  {
    foreach ($controllers as $controller)
    {
      /** @var ControllerPrototype $controller */
      $controller = new $controller;
      $controller->setDomain($this->getDomain());

      call_user_func([$controller, strtolower($request)]);

      foreach ($controller->getViewModels() as $viewModel)
      {

        $viewModel->setDomain($this->getDomain());
        $viewModel->render();
      }
    }
  }

  /**
   * @return mixed
   */
  public function getBaseLayoutPath():string
  {
    return $this->path;
  }

  /**
   * @param $path
   */
  public function setBaseLayoutPath($path)
  {
    $this->path = $path;
  }
}
