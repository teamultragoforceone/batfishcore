<?php


namespace BatFish\Core\Layout;


use BatFish\Core\Domain\DomainAwareInterface;

interface CoreLayoutInterface extends DomainAwareInterface
{
  public function getBaseLayoutPath();

  public function setBaseLayoutPath($path);

  public function render($request, $controllers);
}