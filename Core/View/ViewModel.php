<?php

namespace BatFish\Core\View;

use BatFish\Core\Domain\DomainAwareTrait;
use League\Plates\Engine;

/**
 * Class ViewModel
 * @package BatFish\Core\View
 */
abstract class ViewModel
{
    use DomainAwareTrait;

    /** @var  string */
    protected $viewPath;

    /** @var  string */
    protected $viewName;
    /** @var  ViewModel[] */
    protected $viewModels;

    /**
     * @param $properties
     *
     * @return ViewModel
     */
    public function bind($properties)
    {
        foreach ($properties as $key => $value) {
            $this->{$key} = $value;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getViewPath(): string
    {
        return $this->viewPath;
    }

    /**
     * @param string $viewPath
     *
     * @return ViewModel
     */
    public function setViewPath(string $viewPath): ViewModel
    {
        $this->viewPath = $viewPath;

        return $this;
    }

    public function renderViewModal(ViewModel $viewModel, array $data =[])
    {

        $templates = new Engine(
            '../App/View', 'phtml'
        );
        $data['viewModel'] = $viewModel->setDomain($this->getDomain());
        // Render a template
        echo $templates->render($viewModel->getViewName(), $data);

        /*foreach ($this->getViewModels() as $viewModel) {
            $viewModel->render();
        }*/
    }

    /**
     * @return string
     */
    public function getViewName(): string
    {
        if (!$this->viewName) {
            $this->setViewName(
                substr($this->viewPath, 0, strpos($this->viewPath, "."))
            );
        }

        return $this->viewName;
    }

    /**
     * @param string $viewName
     *
     * @return ViewModel
     */
    public function setViewName(string $viewName): ViewModel
    {
        $this->viewName = $viewName;

        return $this;
    }

    /**
     * @return ViewModel[]
     */
    public function getViewModels(): array
    {
        if (empty($this->viewModels)) {
            $this->viewModels = [];
        }

        return $this->viewModels;
    }

    /**
     * @param ViewModel[] $viewModels
     *
     * @return ViewModel
     */
    public function setViewModels($viewModels)
    {
        $this->viewModels = $viewModels;

        return $this;
    }

    public function render()
    {
        $this->build();

        $templates = new Engine(
            '../App/View', 'phtml'
        );

        // Render a template
        echo $templates->render($this->getViewName(), ['viewModel' => $this]);

        foreach ($this->getViewModels() as $viewModel) {

            $viewModel->setDomain($this->getDomain())->render();
        }
    }

    protected abstract function build();
}

