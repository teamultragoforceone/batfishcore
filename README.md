Batfish Core Framework
---------------------

 #Install
 
 ##Install Composer 
 
 Composer installation is really simple and can be done with a single command:
 
 - `curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer`
 
 
 ## Use Project Stub 
 
 - `wget -O http://batfish.com/lateststub.zip && unzip "$_" -d batfish_procgect`
 - `cd batfish_procgect && composer insall`
 
 ## Composer 
 
 - `mkdir batfish_procgect  && cd "$_"`
 - `composer requier batfish/core`
 
 #Docks
 
 ##Name spacing 
 
 The App is the default namespace for the project stub and is in the App folder 
  
 ## Routes 
 
 By default in the stub there is a `App\Route\routes.php` 
 
 ## Controllers 
 
 Controllers implement 4 mastheads 
  - get() for retrieving a documents 
  - post() for creating new documents
  - put() for updating documents
  - delete() for removing documents
  - _clone() for handling thread duplication for websockets
  
 ## 